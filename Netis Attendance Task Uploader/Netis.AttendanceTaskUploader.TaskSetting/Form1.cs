﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
//using System.Data.SqlClient;
//using System.Data.OleDb;
//using Microsoft.Office.Interop.Access.Dao;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using zkemkeeper;
using System.Data.OleDb;
using Netis.AttendanceTaskUploader.TaskSetting.Classes;
using System.ServiceModel;
using System.Net;

namespace Netis.AttendanceTaskUploader.TaskSetting
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //BindDummyData();
            BindData();
            BindType();
            BindPeriode();
            BindAPI();
        }

        private void CreateAdditionalColumnGridView()
        {
            DataGridViewButtonColumn colNow = new DataGridViewButtonColumn();
            colNow.HeaderText = "Now";
            colNow.Name = "Now";
            colNow.Text = "Now";
            colNow.Width = 50;
            colNow.UseColumnTextForButtonValue = true;

            DataGridViewButtonColumn colEdit = new DataGridViewButtonColumn();
            colEdit.HeaderText = "Edit";
            colEdit.Name = "Edit";
            colEdit.Text = "Edit";
            colEdit.Width = 50;
            colEdit.UseColumnTextForButtonValue = true;

            DataGridViewButtonColumn colDelete = new DataGridViewButtonColumn();
            colDelete.HeaderText = "Delete";
            colDelete.Name = "Delete";
            colDelete.Text = "Delete";
            colDelete.Width = 50;
            colDelete.UseColumnTextForButtonValue = true;

            gvTaskList.Columns.AddRange(new DataGridViewColumn[] { colNow, colEdit, colDelete });
        }

        private void FormatGridView()
        {
            if (gvTaskList.Rows.Count > 0)
            {
                gvTaskList.Columns[0].Visible = false;
                gvTaskList.Columns[1].Width = 300;
                gvTaskList.Columns[1].HeaderText = "Task";
                DataGridViewCellStyle centerstyle = new DataGridViewCellStyle();
                centerstyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //gvTaskList.RowHeadersDefaultCellStyle = centerstyle;
                gvTaskList.ColumnHeadersDefaultCellStyle = centerstyle;
                gvTaskList.Columns[2].HeaderText = "On/Off";
                gvTaskList.Columns[2].Width = 50;
            }
        }

        private void BindDummyData()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("TaskName", typeof(string)));
            dt.Columns.Add(new DataColumn("OnOff", typeof(bool)));

            DataRow dr = dt.NewRow();
            dr[0] = "PT Netika Indonesia";
            dr[1] = true;
            dt.Rows.Add(dr);
            DataRow dr2 = dt.NewRow();
            dr2[0] = "PT JMT Indonesia";
            dr2[1] = true;
            dt.Rows.Add(dr2);

            gvTaskList.DataSource = dt;
            CreateAdditionalColumnGridView();
        }

        private void BindData()
        {
            gvTaskList.DataSource = null;
            gvTaskList.Columns.Clear();
            gvTaskList.Refresh();
            using (OleDbConnection con = new OleDbConnection(ConfigurationManager.ConnectionStrings["hcisConnectionString"].ConnectionString))
            {
                con.Open();
                string sxQuery = @"
                    select 
                        TaskId,
                        TaskName,
                        OnOff 
                    from 
                        hcis_NATU_task
                    where
                        DeletedDate is null";
                OleDbDataAdapter da = new OleDbDataAdapter(sxQuery, con);
                DataTable dt = new DataTable();
                da.Fill(dt);
                gvTaskList.DataSource = dt;
                FormatGridView();
            }
            CreateAdditionalColumnGridView();
        }

        private void BindAPI()
        {
            cmbAPIVersion.DisplayMember = "Text";
            cmbAPIVersion.ValueMember = "Value";

            var items = new[] {
                new { Text = "1", Value = "1" },
                new { Text = "2", Value = "2" }
            };

            cmbAPIVersion.DataSource = items;
        }

        private void BindType()
        {
            using (OleDbConnection con = new OleDbConnection(ConfigurationManager.ConnectionStrings["hcisConnectionString"].ConnectionString))
            {
                con.Open();
                string sxQuery = @"
                    select 
                        SourceTypeId,
                        Description
                    from 
                        hcis_NATU_sourcetype
                    where
                        DeletedDate is null";
                OleDbDataAdapter da = new OleDbDataAdapter(sxQuery, con);
                DataTable dt = new DataTable();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    cmbType.DataSource = dt;
                    cmbType.DisplayMember = "Description";
                    cmbType.ValueMember = "SourceTypeId";
                }
            }
        }

        private void BindPeriode()
        {
            using (OleDbConnection con = new OleDbConnection(ConfigurationManager.ConnectionStrings["hcisConnectionString"].ConnectionString))
            {
                con.Open();
                string sxQuery = @"
                    select 
                        PeriodeId,
                        Description
                    from 
                        hcis_NATU_periode
                    where
                        DeletedDate is null";
                OleDbDataAdapter da = new OleDbDataAdapter(sxQuery, con);
                DataTable dt = new DataTable();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    cmbPeriode.DataSource = dt;
                    cmbPeriode.DisplayMember = "Description";
                    cmbPeriode.ValueMember = "PeriodeId";
                }
            }
        }

        private void BindManufacturer()
        {
            using (OleDbConnection con = new OleDbConnection(ConfigurationManager.ConnectionStrings["hcisConnectionString"].ConnectionString))
            {
                con.Open();
                string sxQuery = @"
                    select 
                        ManufacturerId,
                        Description
                    from 
                        hcis_NATU_manufacturer
                    where
                        SourceTypeId = @id
                        and
                        DeletedDate is null";
                OleDbCommand comm = new OleDbCommand(sxQuery, con);
                int iTypeId = 0;
                int.TryParse(cmbType.SelectedValue.ToString(), out iTypeId);
                comm.Parameters.AddWithValue("@id", iTypeId);
                OleDbDataAdapter da = new OleDbDataAdapter(comm);
                DataTable dt = new DataTable();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    cmbManufacturer.DataSource = dt;
                    cmbManufacturer.DisplayMember = "Description";
                    cmbManufacturer.ValueMember = "ManufacturerId";
                }
            }
        }

        private void BindModel()
        {
            using (OleDbConnection con = new OleDbConnection(ConfigurationManager.ConnectionStrings["hcisConnectionString"].ConnectionString))
            {
                con.Open();
                string sxQuery = @"
                    select 
                        ModelId,
                        Description
                    from 
                        hcis_NATU_model
                    where
                        ManufacturerId = @id
                        and
                        DeletedDate is null";
                OleDbCommand comm = new OleDbCommand(sxQuery, con);
                int iTypeId = 0;
                int.TryParse(cmbManufacturer.SelectedValue.ToString(), out iTypeId);
                comm.Parameters.AddWithValue("@id", iTypeId);
                OleDbDataAdapter da = new OleDbDataAdapter(comm);
                DataTable dt = new DataTable();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    cmbModel.DataSource = dt;
                    cmbModel.DisplayMember = "Description";
                    cmbModel.ValueMember = "ModelId";
                }
            }
        }

        private void BindDetail(int id)
        {
            lblTaskId.Text = id.ToString();
            using (OleDbConnection con = new OleDbConnection(ConfigurationManager.ConnectionStrings["hcisConnectionString"].ConnectionString))
            {
                con.Open();
                //string sxQuery = @"
                //    select 
                //     hcis_NATU_task.[TaskId],
                //     hcis_NATU_task.[TaskName],
                //     hcis_NATU_task.[OnOff],
                //     ts.[SourceTypeId],
                //     ts.[SourceType],
                //     ts.[ManufacturerId],
                //     ts.[Manufacturer],
                //     ts.[ModelId],
                //     ts.[Model],
                //     ts.[IPAddress] as SourceIPAddress,
                //     ts.[Username] as SourceUsername,
                //     ts.[Password] as SourcePassword,
                //     td.[IPAddress] as DestinationIPAddress,
                //     td.[APIVersion] as DestinationAPIVersion,
                //     td.[ClientId] as DestinationClientId,
                //     td.[PassKey] as DestinationPassKey,
                //     tp.[PeriodeId],
                //     tp.[Periode],
                //     tp.[Hours],
                //     tp.[Minutes]
                //    from 
                //     hcis_NATU_task
                //    left join 
                //     (
                //      select 
                //       hcis_NATU_task_source.*,
                //       hcis_NATU_sourcetype.[Description] as SourceType,
                //       hcis_NATU_manufacturer.[Description] as Manufacturer,
                //       hcis_NATU_model.[Description] as Model 
                //      from 
                //       hcis_NATU_task_source 
                //      join hcis_NATU_sourcetype
                //       on hcis_NATU_task_source.[SourceTypeId]=hcis_NATU_sourcetype.[SourceTypeId]
                //      join hcis_NATU_manufacturer
                //       on hcis_NATU_task_source.[ManufacturerId]=hcis_NATU_manufacturer.[ManufacturerId]
                //      join hcis_NATU_model
                //       on hcis_NATU_task_source.[ModelId]=hcis_NATU_model.[ModelId]
                //      where hcis_NATU_task_source.[DeletedDate] is null
                //     ) as ts
                //     on hcis_NATU_task.[TaskId]=ts.[TaskId]
                //    left join 
                //     (
                //      select * from hcis_NATU_task_destination where [DeletedDate] is null
                //     ) as td
                //     on hcis_NATU_task.[TaskId]=td.[TaskId]
                //    left join 
                //     (
                //      select 
                //       hcis_NATU_task_periode.[TaskId],
                //       hcis_NATU_task_periode.[PeriodeId],
                //       hcis_NATU_periode.[Description] as Periode,
                //       hcis_NATU_task_periode.[Hours],
                //       hcis_NATU_task_periode.[Minutes] 
                //      from 
                //       hcis_NATU_task_periode
                //      join hcis_NATU_periode
                //       on hcis_NATU_task_periode.[PeriodeId] = hcis_NATU_periode.[PeriodeId]
                //      where hcis_NATU_task_periode.[DeletedDate] is null
                //     ) as tp
                //     on hcis_NATU_task.[TaskId]=tp.[TaskId];";
                string sxQuery = @"
                    select 
	                    [TaskId],
	                    [TaskName],
	                    [OnOff],
	                    [SourceTypeId],
	                    [SourceType],
	                    [ManufacturerId],
	                    [Manufacturer],
	                    [ModelId],
	                    [Model],
	                    [SourceIPAddress],
	                    [SourceUsername],
	                    [SourcePassword],
                        [NIPColumn],
                        [MachineConfig],
                        [NIPConfig],
                        [DeleteAfterProcess],
	                    [DestinationIPAddress],
	                    [DestinationAPIVersion],
	                    [ClientId] as [DestinationClientId],
	                    [PassKey] as [DestinationPassKey],
	                    [PeriodeId],
	                    [Periode],
	                    [Hours],
	                    [Minutes]
                    from 
	                    vwTasks
                    where DeletedDate is null and TaskId = "+id.ToString()+";";
                OleDbDataAdapter da = new OleDbDataAdapter(sxQuery, con);
                DataTable dt = new DataTable();
                da.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    txtTaskName.Text = dt.Rows[0]["TaskName"].ToString();
                    cmbType.SelectedValue = dt.Rows[0]["SourceTypeId"].ToString();
                    BindManufacturer();
                    cmbManufacturer.SelectedValue = dt.Rows[0]["ManufacturerId"].ToString();
                    BindModel();
                    cmbModel.SelectedValue = dt.Rows[0]["ModelId"].ToString();
                    txtIPAddress.Text = dt.Rows[0]["SourceIPAddress"].ToString();
                    txtUsername.Text = dt.Rows[0]["SourceUsername"].ToString();
                    txtPassword.Text = dt.Rows[0]["SourcePassword"].ToString();
                    txtServerIPAddress.Text = dt.Rows[0]["DestinationIPAddress"].ToString();
                    cmbAPIVersion.SelectedValue = dt.Rows[0]["DestinationAPIVersion"].ToString();
                    txtClientId.Text = dt.Rows[0]["DestinationClientId"].ToString();
                    txtPassKey.Text = dt.Rows[0]["DestinationPassKey"].ToString();
                    cmbPeriode.SelectedValue = dt.Rows[0]["PeriodeId"].ToString();
                    numHours.Value = Convert.ToDecimal(dt.Rows[0]["Hours"]);
                    numMinutes.Value = Convert.ToDecimal(dt.Rows[0]["Minutes"]);
                    int iNIPColumn = 1;
                    if(dt.Rows[0]["NIPColumn"]!= null && dt.Rows[0]["NIPColumn"] != DBNull.Value)
                    {
                        iNIPColumn = Convert.ToInt16(dt.Rows[0]["NIPColumn"]);
                    }
                    if (iNIPColumn==1)
                    {
                        radId.Checked = true;
                    }
                    else
                    {
                        radName.Checked = true;
                    }
                    txtMachineConfig.Text = dt.Rows[0]["MachineConfig"] != null && dt.Rows[0]["MachineConfig"] != DBNull.Value ? dt.Rows[0]["MachineConfig"].ToString() : String.Empty;
                    txtNIPConfig.Text = dt.Rows[0]["NIPConfig"] != null && dt.Rows[0]["NIPConfig"] != DBNull.Value ? dt.Rows[0]["NIPConfig"].ToString() : String.Empty;
                    chkDeleteAfterProcess.Checked = Convert.ToBoolean(dt.Rows[0]["DeleteAfterProcess"]);
                }
            }
        }

        private FingerDevice BindFingerDevice()
        {
            FingerDevice _fd = new FingerDevice();
            _fd.TypeId = (int)cmbType.SelectedValue;
            _fd.Type = cmbType.SelectedText;
            _fd.ManufacturerId = (int)cmbManufacturer.SelectedValue;
            _fd.Manufacturer = cmbManufacturer.SelectedText;
            _fd.ModelId = (int)cmbModel.SelectedValue;
            _fd.Model = cmbModel.SelectedText;
            _fd.IPAddress = txtIPAddress.Text.Trim();
            _fd.Username = txtUsername.Text;
            _fd.Password = txtPassword.Text;
            return _fd;
        }

        private void gvTaskList_Paint(object sender, PaintEventArgs e)
        {
            DataGridView sndr = (DataGridView)sender;
            if (sndr.Rows.Count == 0)
            {
                using (Graphics grfx = e.Graphics)
                {
                    grfx.FillRectangle(Brushes.White, new Rectangle(new Point(), new Size(sndr.Width, 25)));
                    grfx.DrawString("No data returned", new Font("Arial", 12), Brushes.Black, new PointF(3, 3));
                }
            }
        }

        private void gvTaskList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int id = 0;
            bool res = int.TryParse(gvTaskList.Rows[e.RowIndex].Cells[0].Value.ToString(), out id);
            if (res == false)
            {
                id = 0;
            }
            if (gvTaskList.Columns[e.ColumnIndex].Name == "Now")
            {
                string message = "Id : " + id;
                BindDetail(id);
                Now(BindFingerDevice());
                ClearInput();
            }
            else if (gvTaskList.Columns[e.ColumnIndex].Name == "Edit")
            {
                BindDetail(id);
                //txtTaskName.Enabled = false;
                //pnlAddTask.Visible = true;
                //gvTaskList.Visible = false;
                gvTaskList.Visible = false;
                pnlAddTask.Visible = true;
                txtTaskName.Enabled = true;
                txtTaskName.Visible = true;
                btnSaveTask.Visible = true;
                btnCancel.Visible = true;
                label2.Visible = false;
                btnAdd.Visible = false;
                label3.Visible = true;
            }
            else if (gvTaskList.Columns[e.ColumnIndex].Name == "Delete")
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure to delete this Task?", "Netis Attendance Task Uploader", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    Delete(id);
                }
                ClearInput();
                BindData();
            }
            else if (gvTaskList.Columns[e.ColumnIndex].Name == "OnOff")
            {
                bool isOnOff = (bool)gvTaskList.Rows[e.RowIndex].Cells[2].Value;
                SetOnOff(id, !isOnOff);
                ClearInput();
                BindData();
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            txtTaskName.Enabled = true;
            txtTaskName.Visible = true;
            gvTaskList.Visible = false;
            pnlAddTask.Visible = true;
            btnSaveTask.Visible = true;
            btnCancel.Visible = true;
            label2.Visible = false;
            btnAdd.Visible = false;
            label3.Visible = true;
        }

        private void btnSaveTask_Click(object sender, EventArgs e)
        {
            CheckInput();
            if (!errorProvider1.HasErrors())
            {
                #region add new
                if (string.IsNullOrWhiteSpace(lblTaskId.Text))
                {
                    AddNew();
                }
                #endregion
                #region edit
                else
                {
                    Edit();
                    txtTaskName.Enabled = true;
                }
                #endregion

                ClearInput();
                gvTaskList.Visible = true;
                pnlAddTask.Visible = true;
                txtTaskName.Enabled = false;
                txtTaskName.Visible = false;
                gvTaskList.Visible = true;
                btnSaveTask.Visible = false;
                btnCancel.Visible = false;
                label2.Visible = true;
                btnAdd.Visible = true;
                label3.Visible = false;

                BindData();
            }

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            ClearInput();
            gvTaskList.Visible = true;
            pnlAddTask.Visible = true;
            txtTaskName.Enabled = false;
            txtTaskName.Visible = false;
            gvTaskList.Visible = true;
            btnSaveTask.Visible = false;
            btnCancel.Visible = false;
            label2.Visible = true;
            btnAdd.Visible = true;
            label3.Visible = false;
        }

        private void cmbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindManufacturer();
        }

        private void cmbManufacturer_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindModel();
        }

        private void AddNew()
        {
            try
            {
                using (OleDbConnection con = new OleDbConnection(ConfigurationManager.ConnectionStrings["hcisConnectionString"].ConnectionString))
                {
                    con.Open();
                    //string sxQuery = @"
                    // declare @id as int
                    //    begin transaction [Tran1]

                    //    begin try

                    //     if not exists(select 1 from hcis_NATU_task where lower(TaskName) = lower(@TaskName) and DeletedDate is null)
                    //     begin

                    //     insert into hcis_NATU_task
                    //     (TaskName, OnOff, CreatedDate)
                    //     values
                    //     (@TaskName, 1, getdate())

                    //     select @id = SCOPE_IDENTITY()

                    //     insert into hcis_NATU_task_source
                    //     (TaskId, SourceTypeId, ManufacturerId, ModelId, IPAddress, Username, Password, CreatedDate)
                    //     values
                    //     (@id, @SourceTypeId, @ManufacturerId, @ModelId, @IPAddress, @Username, @Password, getdate())

                    //     insert into hcis_NATU_task_destination
                    //     (TaskId, IPAddress, APIVersion, ClientId, PassKey, CreatedDate)
                    //     values
                    //     (@id, @ServerIPAddress, @APIVersion, @ClientId, @PassKey, getdate())

                    //     insert into hcis_NATU_task_periode
                    //     (TaskId, PeriodeId, Hours, Minutes, CreatedDate)
                    //     values
                    //     (@id, @PeriodeId, @Hours, @Minutes, getdate())

                    //     commit transaction [Tran1]

                    //     select @id
                    //     end
                    //     else
                    //     begin
                    //     RAISERROR ('Data Already Exists',
                    //            16, 127)
                    //     end
                    //    end try
                    //    begin catch
                    //     rollback transaction [Tran1]
                    //        select ERROR_MESSAGE()
                    //    end catch";
                    string sxQuery = string.Empty;
                    OleDbCommand comm;
                    sxQuery = @"
	                        insert into hcis_NATU_task
	                        (TaskName, OnOff, CreatedDate)
	                        values
	                        (@TaskName, 1, @DateNow);";
                    comm = new OleDbCommand(sxQuery, con);
                    comm.Parameters.AddWithValue("@TaskName", txtTaskName.Text);
                    comm.Parameters.AddWithValue("@DateNow", DateTime.Now.ToString("yyyy-MM-dd"));
                    comm.ExecuteNonQuery();

                   
                    sxQuery = @"
	                        select top 1 TaskId from hcis_NATU_task order by TaskId desc;";
                    comm = new OleDbCommand(sxQuery, con);
                    int iNewTaskId = -1;
                    //OleDbDataReader dr = comm.ExecuteReader();
                    string value = comm.ExecuteScalar().ToString();
                    bool a = int.TryParse(value, out iNewTaskId);


                    OleDbCommand comm1;
                    sxQuery = "insert into hcis_NATU_task_source ";
                    sxQuery = sxQuery + " (TaskId, SourceTypeId, ManufacturerId, ModelId, IPAddress, [Username], [Password], [NIPColumn], [MachineConfig], [NIPConfig], [DeleteAfterProcess], CreatedDate) ";
                    sxQuery = sxQuery + " values (@TaskId, @SourceTypeId, @ManufacturerId, @ModelId, @IPAddress, @Username, @Password, @NIPColumn, @MachineConfig, @NIPConfig, @DeleteAfterProcess, @DateNow);";
                    comm1 = new OleDbCommand(sxQuery, con);
                    comm1.Parameters.AddWithValue("@TaskId", iNewTaskId);
                    int iTypeId = 0;
                    int.TryParse(cmbType.SelectedValue.ToString(), out iTypeId);
                    comm1.Parameters.AddWithValue("@SourceTypeId", iTypeId);
                    int iManufacturerId = 0;
                    int.TryParse(cmbManufacturer.SelectedValue.ToString(), out iManufacturerId);
                    comm1.Parameters.AddWithValue("@ManufacturerId", iManufacturerId);
                    int iModelId = 0;
                    int.TryParse(cmbModel.SelectedValue.ToString(), out iModelId);
                    comm1.Parameters.AddWithValue("@ModelId", iModelId);
                    comm1.Parameters.AddWithValue("@IPAddress", txtIPAddress.Text);
                    comm1.Parameters.AddWithValue("@Username", txtUsername.Text);
                    comm1.Parameters.AddWithValue("@Password", txtPassword.Text);
                    int iNIPColumn = 0;
                    iNIPColumn = radId.Checked ? 1 : 2;
                    comm1.Parameters.AddWithValue("@NIPColumn", iNIPColumn);
                    comm1.Parameters.AddWithValue("@MachineConfig", txtMachineConfig.Text);
                    comm1.Parameters.AddWithValue("@NIPConfig", txtNIPConfig.Text);
                    comm1.Parameters.AddWithValue("@DeleteAfterProcess", chkDeleteAfterProcess.Checked);
                    comm1.Parameters.AddWithValue("@DateNow", DateTime.Now.ToString("yyyy-MM-dd"));
                    comm1.ExecuteNonQuery();

                    sxQuery = @"
	                        insert into hcis_NATU_task_destination
	                        (TaskId, IPAddress, APIVersion, ClientId, PassKey, CreatedDate)
	                        values
	                        (@TaskId, @ServerIPAddress, @APIVersion, @ClientId, @PassKey, @DateNow);";
                    comm = new OleDbCommand(sxQuery, con);
                    comm.Parameters.AddWithValue("@TaskId", iNewTaskId);
                    comm.Parameters.AddWithValue("@ServerIPAddress", txtServerIPAddress.Text);
                    int iAPIVersionId = 0;
                    int.TryParse(cmbAPIVersion.SelectedValue.ToString(), out iAPIVersionId);
                    comm.Parameters.AddWithValue("@APIVersion", iAPIVersionId);
                    comm.Parameters.AddWithValue("@ClientId", txtClientId.Text);
                    comm.Parameters.AddWithValue("@PassKey", txtPassKey.Text);
                    comm.Parameters.AddWithValue("@DateNow", DateTime.Now.ToString("yyyy-MM-dd"));
                    comm.ExecuteNonQuery();

                    sxQuery = @"
	                        insert into hcis_NATU_task_periode
	                        (TaskId, PeriodeId, Hours, Minutes, CreatedDate)
	                        values
	                        (@TaskId, @PeriodeId, @Hours, @Minutes, @DateNow);";
                    comm = new OleDbCommand(sxQuery, con);
                    int iPeriodeId = 0;
                    int.TryParse(cmbPeriode.SelectedValue.ToString(), out iPeriodeId);
                    comm.Parameters.AddWithValue("@TaskId", iNewTaskId);
                    comm.Parameters.AddWithValue("@PeriodeId", iPeriodeId);
                    comm.Parameters.AddWithValue("@Hours", numHours.Value);
                    comm.Parameters.AddWithValue("@Minutes", numMinutes.Value);
                    comm.Parameters.AddWithValue("@DateNow", DateTime.Now.ToString("yyyy-MM-dd"));
                    comm.ExecuteNonQuery();

                    
                    //iNewTaskId = (int)comm.ExecuteScalar();
                    if (iNewTaskId > 0)
                    {
                        MessageBox.Show("Task already added");
                    }
                    //else
                    //{
                    //    MessageBox.Show(value);
                    //}
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to add task. Error: " + ex.Message);
            }
        }

        private void Edit()
        {
            int iTaskId = -1;
            bool res = int.TryParse(lblTaskId.Text, out iTaskId);
            if(res)
            {
                try
                {
                    using (OleDbConnection con = new OleDbConnection(ConfigurationManager.ConnectionStrings["hcisConnectionString"].ConnectionString))
                    {
                        con.Open();
                        //string sxQuery = @"
                        //begin transaction [Tran1]

                        //begin try

                        // update hcis_NATU_task_source set
                        //        SourceTypeId = @SourceTypeId, 
                        //        ManufacturerId = @ManufacturerId, 
                        //        ModelId = @ModelId, 
                        //        IPAddress = @IPAddress, 
                        //        Username = @Username, 
                        //        Password = @Password, 
                        //        UpdatedDate = getdate()
                        // where TaskId = @TaskId

                        // update hcis_NATU_task_destination set
                        //     IPAddress = @ServerIPAddress, 
                        //        APIVersion = @APIVersion, 
                        //        ClientId = @ClientId, 
                        //        PassKey = @PassKey, 
                        //        UpdatedDate = getdate()
                        // where TaskId = @TaskId

                        // update hcis_NATU_task_periode set
                        //     PeriodeId = @PeriodeId, 
                        //        Hours = @Hours, 
                        //        Minutes = @Minutes, 
                        //        UpdatedDate = getdate()
                        // where TaskId = @TaskId

                        // commit transaction [Tran1]

                        //end try
                        //begin catch
                        // rollback transaction [Tran1]
                        //end catch";
                        string sxQuery = string.Empty;
                        OleDbCommand comm;
                        int iRowAffected = 0;
                        int iTypeId = 0;
                        int.TryParse(cmbType.SelectedValue.ToString(), out iTypeId);
                        int iManufacturerId = 0;
                        int.TryParse(cmbManufacturer.SelectedValue.ToString(), out iManufacturerId);
                        int iModelId = 0;
                        int.TryParse(cmbModel.SelectedValue.ToString(), out iModelId);
                        int iNIPColumn = 0;
                        iNIPColumn = radId.Checked ? 1 : 2;
                        string sxIsDeleteAfterProcess = chkDeleteAfterProcess.Checked ? "1" : "0";
                        sxQuery = @"
	                        update hcis_NATU_task_source set
                                [SourceTypeId] = "+iTypeId.ToString()+@", 
                                [ManufacturerId] = "+iManufacturerId.ToString()+@", 
                                [ModelId] = "+iModelId.ToString()+@", 
                                [IPAddress] = '"+ txtIPAddress.Text + @"', 
                                [Username] = '"+ txtUsername.Text + @"', 
                                [Password] = '"+ txtPassword.Text + @"', 
                                [NIPColumn] = '"+ iNIPColumn.ToString() + @"', 
                                [MachineConfig] = '"+ txtMachineConfig.Text + @"', 
                                [NIPConfig] = '" + txtNIPConfig.Text + @"', 
                                [DeleteAfterProcess] = '" + sxIsDeleteAfterProcess + @"', 
                                [UpdatedDate] = '" + DateTime.Now.ToString("yyyy-MM-dd") + @"'
	                        where [TaskId] = "+ iTaskId .ToString()+ @";";
                        comm = new OleDbCommand(sxQuery, con);
                        //comm.Parameters.AddWithValue("@TaskId", iTaskId);
                        //comm.Parameters.AddWithValue("@SourceTypeId", iTypeId);
                        //comm.Parameters.AddWithValue("@ManufacturerId", iManufacturerId);
                        //comm.Parameters.AddWithValue("@ModelId", iModelId);
                        //comm.Parameters.AddWithValue("@IPAddress", txtIPAddress.Text);
                        //comm.Parameters.AddWithValue("@Username", txtUsername.Text);
                        //comm.Parameters.AddWithValue("@Password", txtPassword.Text);
                        //comm.Parameters.AddWithValue("@DateNow", DateTime.Now.ToString("yyyy-MM-dd"));
                        iRowAffected += comm.ExecuteNonQuery();

                        int iAPIVersionId = 0;
                        int.TryParse(cmbAPIVersion.SelectedValue.ToString(), out iAPIVersionId);
                        sxQuery = @"
                            update hcis_NATU_task_destination set
	                            [IPAddress] = '"+ txtServerIPAddress.Text + @"', 
                                [APIVersion] = "+ iAPIVersionId.ToString()+ @", 
                                [ClientId] = '"+ txtClientId.Text + @"', 
                                [PassKey] = '"+ txtPassKey.Text + @"', 
                                [UpdatedDate] = '"+ DateTime.Now.ToString("yyyy-MM-dd") + @"'
	                        where [TaskId] = "+ iTaskId.ToString() + @";";
                        comm = new OleDbCommand(sxQuery, con);
                        //comm.Parameters.AddWithValue("@ServerIPAddress", txtServerIPAddress.Text);
                        //comm.Parameters.AddWithValue("@APIVersion", iAPIVersionId);
                        //comm.Parameters.AddWithValue("@ClientId", txtClientId.Text);
                        //comm.Parameters.AddWithValue("@PassKey", txtPassKey.Text);
                        //comm.Parameters.AddWithValue("@DateNow", DateTime.Now.ToString("yyyy-MM-dd"));
                        //comm.Parameters.AddWithValue("@TaskId", iTaskId);
                        iRowAffected += comm.ExecuteNonQuery();

                        int iPeriodeId = 0;
                        int.TryParse(cmbPeriode.SelectedValue.ToString(), out iPeriodeId);
                        sxQuery = @"
                            update hcis_NATU_task_periode set
	                            PeriodeId = "+ iPeriodeId.ToString() + @", 
                                Hours = "+ numHours.Value.ToString() + @", 
                                Minutes = "+ numMinutes.Value.ToString() + @", 
                                UpdatedDate = '"+ DateTime.Now.ToString("yyyy-MM-dd") + @"'
	                        where TaskId = "+ iTaskId.ToString() + @";";
                        comm = new OleDbCommand(sxQuery, con);
                        //comm.Parameters.AddWithValue("@PeriodeId", iPeriodeId);
                        //comm.Parameters.AddWithValue("@Hours", numHours.Value);
                        //comm.Parameters.AddWithValue("@Minutes", numMinutes.Value);
                        //comm.Parameters.AddWithValue("@DateNow", DateTime.Now.ToString("yyyy-MM-dd"));
                        //comm.Parameters.AddWithValue("@TaskId", iTaskId);
                        iRowAffected += comm.ExecuteNonQuery();

                        if (iRowAffected > 0)
                        {
                            MessageBox.Show("Task already edited");
                        }
                        else
                        {
                            MessageBox.Show("Failed to edit task");
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Failed to edit task. Error: " + ex.Message);
                }
            }
        }

        private void Now(FingerDevice fd)
        {
            FingerControlClass fcc = new FingerControlClass();
            NATUServices.NATUServicesClient _natuClient = new NATUServices.NATUServicesClient();
            try
            {
                string sxNewURI = "http://"+txtServerIPAddress.Text+"/NATUServices.svc";
                //string sxNewURI = "http://localhost/NATUServices.svc";
                _natuClient.Endpoint.Address = new EndpointAddress(new Uri(sxNewURI),
                    _natuClient.Endpoint.Address.Identity, _natuClient.Endpoint.Address.Headers);
                _natuClient.Open();
                ServicePoint servicePoint = ServicePointManager.FindServicePoint(new Uri(sxNewURI));
                if (servicePoint.Expect100Continue == true)
                { //should happen only once for each URL
                    servicePoint.Expect100Continue = false;
                }
                bool isValid = _natuClient.ClientValidation(txtClientId.Text, txtPassKey.Text);
                //MessageBox.Show(isValid.ToString());
                int iNIPColumn = radId.Checked ? 1 : 2;
                string sxMachineConfig = txtMachineConfig.Text;
                string sxNIPConfig = txtNIPConfig.Text;
                if (isValid)
                {
                    bool isSuccess = false;
                    bool isSuccess2 = false;
                    bool isSuccessNIP = false;
                    string sxErrorMessage = string.Empty;
                    DataTable dtNIP = new DataTable();//_natuClient.GetPersonsNIP();
                    bool isConnect = ConfigurationManager.AppSettings["DevMode"].ToString() == "true" || fcc.ConnectDevice_Net(fd, ref sxErrorMessage);
                    //if (fcc.ConnectDevice_Net(fd, ref sxErrorMessage))
                    if (isConnect)
                    {
                        //if(fcc.MachineAuthentication(fd, ref sxErrorMessage))
                        //{
                        int iDeviceId = ConfigurationManager.AppSettings["DevMode"].ToString() != "true" ? fcc.GetDeviceId(fd, ref sxErrorMessage) : 1;
                        isSuccessNIP = _natuClient.UpdateNIPConfig(txtClientId.Text, sxNIPConfig, sxMachineConfig);
                        DataTable dt= new DataTable();
                        if (ConfigurationManager.AppSettings["DevMode"].ToString() != "true")
                        {
                            dt = fcc.GetLogData(fd, iNIPColumn, sxMachineConfig, sxNIPConfig, dtNIP, ref sxErrorMessage);
                        }

                        dt.TableName = "Data";
                        //-->Testing block
                        if (ConfigurationManager.AppSettings["DevMode"].ToString()=="true")
                        {
                            ManualFillData(dt, iDeviceId, iNIPColumn, sxMachineConfig, sxNIPConfig);  //just for debugging
                        }
                        //<--Testing block
                        //isSuccess = _natuClient.UploadData(txtClientId.Text, DateTime.Now, dt, iDeviceId, fd.IPAddress);
                        string sxMessage = _natuClient.UploadDataWithErrorMessage(txtClientId.Text, DateTime.Now, dt, iDeviceId, fd.IPAddress);
                        isSuccess = sxMessage.Contains("SUCCESS");
                        if (chkDeleteAfterProcess.Checked)
                        {
                            isSuccess2 = fcc.ClearLogData(fd, ref sxErrorMessage);
                        }
                        else
                        {
                            isSuccess2 = true;
                        }
                        if (isSuccess && isSuccess2 && isSuccessNIP)
                        {
                            MessageBox.Show("Success Upload Data");
                            UpdateTaskLastUpdatedDate(Convert.ToInt32(lblTaskId.Text));
                        }
                        else
                        {
                            if(!isSuccess)
                            {
                                //sxErrorMessage += "error in web service(data count: "+dt.Rows.Count.ToString()+"); ";
                                sxErrorMessage += "message from web service: " + sxMessage.Split(new string[] { "]=[" }, StringSplitOptions.None)[1];
                            }
                            if(!isSuccess2)
                            {
                                sxErrorMessage += "error in clear log";
                            }
                            if(!isSuccessNIP)
                            {
                                sxErrorMessage += "error in UpdateNIPConfig";
                            }
                            throw new Exception("Error when uploading data : " + sxErrorMessage);
                        }
                        //}
                        //else
                        //{
                        //    throw new Exception("this username is not authorize to access this machine : " + sxErrorMessage);
                        //}
                    }
                    else
                    {
                        throw new Exception(sxErrorMessage);
                    }
                }
                else
                {
                    throw new Exception("Finger Device is not valid or is not active anymore");
                }
                
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                fcc.DisconnectDevice_Net(fd);
                _natuClient.Close();
                //_natuClient.Abort();
            }
        }

        /// <summary>
        /// Just for debugging
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="iDeviceId"></param>
        /// <param name="iNIPColumn"></param>
        /// <param name="sxMachineConfig"></param>
        /// <param name="sxNIPConfig"></param>
        private void ManualFillData(DataTable dt, int iDeviceId, int iNIPColumn, string sxMachineConfig, string sxNIPConfig)
        {
            iDeviceId = 1;
        


            if (dt.Rows.Count==0)
            {

                dt.Columns.Add("NIP", typeof(string));
                dt.Columns.Add("LogDate", typeof(string));
                dt.Columns.Add("LogTime", typeof(string));
                dt.Columns.Add("LogType", typeof(string));
                OpenFileDialog ofd = new OpenFileDialog();
                DialogResult tes =ofd.ShowDialog();
                if (ofd.FileName.Length>0)
                {
                    System.IO.StreamReader reader = new System.IO.StreamReader(ofd.FileName);
                    string line = string.Empty;
                    int index = 0;

                    while ((line = reader.ReadLine()) != null)
                    {
                        if (index > 1 && line.Length> 50) //start reading from line 2
                        {
                            DateTime dtLogIn;
                            DateTime dtLogOut;
                            if (DateTime.TryParse(line.Substring(91, 5), out dtLogIn))
                            {
                                string LogDate = Convert.ToDateTime(line.Substring(50, 12)).ToString("yyyy-MM-dd");
                                //string LogTimeIn = Convert.ToDateTime(line.Substring(91, 5)).ToString("HH':'mm':'ss");
                                //string LogTimeOut = Convert.ToDateTime(line.Substring(101, 5)).ToString("HH':'mm':'ss");
                                DataRow dr1 = dt.NewRow();
                                dr1["NIP"] = line.Substring(9, 10).Trim();
                                dr1["LogType"] = 0;
                                dr1["LogDate"] = LogDate;
                                dr1["LogTime"] = dtLogIn.ToString("HH':'mm':'ss"); 
                                dt.Rows.Add(dr1);

                                //if he/she went home.
                                if (DateTime.TryParse(line.Substring(101, 5), out dtLogOut))
                                {
                                    DataRow dr2 = dt.NewRow();
                                    dr2["NIP"] = line.Substring(9, 10).Trim();
                                    dr2["LogType"] = 0;
                                    dr2["LogDate"] = LogDate;
                                    dr2["LogTime"] = dtLogOut.ToString("HH':'mm':'ss"); 
                                    dt.Rows.Add(dr2);
                                }

                            }
                        }

                        index++;
                    }
                }
                
                


                
                //dr["NIP"] = "141088008";
                //dr["LogType"] = 1;
                //dr["LogDate"] = DateTime.Now.ToString("yyyy-MM-dd");
                //dr["LogTime"] = DateTime.Now.ToString("HH':'mm':'ss");
                //dt.Rows.Add(dr);
                //iDeviceId = 1;

                //dr = dt.NewRow();
                //dr["NIP"] = "141088008";
                //dr["LogType"] = 1;
                //dr["LogDate"] = DateTime.Now.ToString("yyyy-MM-dd");
                //dr["LogTime"] = DateTime.Now.AddHours(7).ToString("HH':'mm':'ss");
                //dt.Rows.Add(dr);

                //dr = dt.NewRow();
                //dr["NIP"] = "141088008";
                //dr["LogType"] = 1;
                //dr["LogDate"] = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
                //dr["LogTime"] = DateTime.Now.ToString("HH':'mm':'ss");
                //dt.Rows.Add(dr);

                //dr = dt.NewRow();
                //dr["NIP"] = "141088008";
                //dr["LogType"] = 1;
                //dr["LogDate"] = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
                //dr["LogTime"] = DateTime.Now.AddHours(7).ToString("HH':'mm':'ss");
                //dt.Rows.Add(dr);
            }

        }

        private void Delete(int id)
        {
            try
            {
                using (OleDbConnection con = new OleDbConnection(ConfigurationManager.ConnectionStrings["hcisConnectionString"].ConnectionString))
                {
                    con.Open();
                    //string sxQuery = @"
                    //    begin transaction [Tran1]
                    //    begin try
                    //        update hcis_NATU_task
                    //        set DeletedDate = getdate()
                    //        where TaskId = @TaskId

                    //        update hcis_NATU_task_source
                    //        set DeletedDate = getdate()
                    //        where TaskId = @TaskId

                    //        update hcis_NATU_task_destination
                    //        set DeletedDate = getdate()
                    //        where TaskId = @TaskId

                    //        update hcis_NATU_task_periode
                    //        set DeletedDate = getdate()
                    //        where TaskId = @TaskId

                    //     commit transaction [Tran1]

                    //    end try
                    //    begin catch
                    //     rollback transaction [Tran1]
                    //        select ERROR_MESSAGE()
                    //    end catch";
                    string sxQuery = string.Empty;
                    OleDbCommand comm;
                    int iRowAffected = 0;
                    sxQuery = @"
	                        update hcis_NATU_task
                            set DeletedDate = '"+ DateTime.Now.ToString("yyyy-MM-dd") + @"'
                            where TaskId = "+id.ToString()+@";";
                    comm = new OleDbCommand(sxQuery, con);
                    //comm.Parameters.AddWithValue("@TaskId", id);
                    //comm.Parameters.AddWithValue("@DateNow", DateTime.Now.ToString("yyyy-MM-dd"));
                    iRowAffected += comm.ExecuteNonQuery();

                    sxQuery = @"
	                        update hcis_NATU_task_source
                            set DeletedDate = '" + DateTime.Now.ToString("yyyy-MM-dd") + @"'
                            where TaskId = " + id.ToString() + @";";
                    comm = new OleDbCommand(sxQuery, con);
                    //comm.Parameters.AddWithValue("@TaskId", id);
                    //comm.Parameters.AddWithValue("@DateNow", DateTime.Now.ToString("yyyy-MM-dd"));
                    iRowAffected += comm.ExecuteNonQuery();

                    sxQuery = @"
	                        update hcis_NATU_task_destination
                            set DeletedDate = '" + DateTime.Now.ToString("yyyy-MM-dd") + @"'
                            where TaskId = " + id.ToString() + @";";
                    comm = new OleDbCommand(sxQuery, con);
                    //comm.Parameters.AddWithValue("@TaskId", id);
                    //comm.Parameters.AddWithValue("@DateNow", DateTime.Now.ToString("yyyy-MM-dd"));
                    iRowAffected += comm.ExecuteNonQuery();
                    iRowAffected += comm.ExecuteNonQuery();

                    sxQuery = @"
	                        update hcis_NATU_task_periode
                            set DeletedDate = '" + DateTime.Now.ToString("yyyy-MM-dd") + @"'
                            where TaskId = " + id.ToString() + @";";
                    comm = new OleDbCommand(sxQuery, con);
                    //comm.Parameters.AddWithValue("@TaskId", id);
                    //comm.Parameters.AddWithValue("@DateNow", DateTime.Now.ToString("yyyy-MM-dd"));
                    iRowAffected += comm.ExecuteNonQuery();
                    iRowAffected += comm.ExecuteNonQuery();

                    if (iRowAffected > 0)
                    {
                        MessageBox.Show("Task already deleted");
                    }
                    else
                    {
                        MessageBox.Show("Failed to delete task");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to delete task. Error: " + ex.Message);
            }
        }

        private void SetOnOff(int id, bool OnOff)
        {
            try
            {
                using (OleDbConnection con = new OleDbConnection(ConfigurationManager.ConnectionStrings["hcisConnectionString"].ConnectionString))
                {
                    con.Open();
                    string sxOnOff = OnOff ? "1" : "0";
                    string sxQuery = @"
                            update hcis_NATU_task
                            set OnOff = "+sxOnOff+", UpdatedDate = '" + DateTime.Now.ToString("yyyy-MM-dd") + @"'
                            where TaskId = " + id.ToString() + ";";
                    OleDbCommand comm = new OleDbCommand(sxQuery, con);
                    //comm.Parameters.AddWithValue("@OnOff", OnOff);
                    //comm.Parameters.AddWithValue("@TaskId", id);
                    int iRowAffected = 0;
                    iRowAffected = comm.ExecuteNonQuery();
                    string status = OnOff ? "set on" : "set off";
                    if (iRowAffected > 0)
                    {
                        MessageBox.Show("Task already " + status);
                    }
                    else
                    {
                        MessageBox.Show("Failed to "+status+" task");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to set task. Error: " + ex.Message);
            }
        }

        private void CheckInput()
        {
            errorProvider1.ReleaseAllError();
            if (string.IsNullOrWhiteSpace(txtTaskName.Text))
            {
                errorProvider1.SetErrorWithCount(txtTaskName, "Task Name can not be empty");
            }
            else
            {
                errorProvider1.SetErrorWithCount(txtTaskName, string.Empty);
            }
            if (string.IsNullOrWhiteSpace(txtIPAddress.Text))
            {
                errorProvider1.SetErrorWithCount(txtIPAddress, "IP Address can not be empty");
            }
            else
            {
                errorProvider1.SetErrorWithCount(txtIPAddress, string.Empty);
            }
            if (cmbModel.SelectedIndex < 0)
            {
                errorProvider1.SetErrorWithCount(cmbModel, "Please select model");
            }
            else
            {
                errorProvider1.SetErrorWithCount(cmbModel, string.Empty);
            }
            if (string.IsNullOrWhiteSpace(txtUsername.Text))
            {
                errorProvider1.SetErrorWithCount(txtUsername, "Device Id can not be empty");
            }
            else
            {
                errorProvider1.SetErrorWithCount(txtUsername, string.Empty);
            }
            int iDeviceId = -1;
            if (!int.TryParse(txtUsername.Text, out iDeviceId))
            {
                errorProvider1.SetErrorWithCount(txtUsername, "Device Id must numeric");
            }
            else
            {
                errorProvider1.SetErrorWithCount(txtUsername, string.Empty);
            }
            if (string.IsNullOrWhiteSpace(txtPassword.Text))
            {
                errorProvider1.SetErrorWithCount(txtPassword, "Device Password can not be empty");
            }
            else
            {
                errorProvider1.SetErrorWithCount(txtPassword, string.Empty);
            }
            int iPassword = -1;
            if(!int.TryParse(txtPassword.Text, out iPassword))
            {
                errorProvider1.SetErrorWithCount(txtPassword, "Device Password must numeric");
            }
            else
            {
                errorProvider1.SetErrorWithCount(txtPassword, string.Empty);
            }
            if (string.IsNullOrWhiteSpace(txtServerIPAddress.Text))
            {
                errorProvider1.SetErrorWithCount(txtServerIPAddress, "Server's IP Address can not be empty");
            }
            else
            {
                errorProvider1.SetErrorWithCount(txtServerIPAddress, string.Empty);
            }
            if (cmbAPIVersion.SelectedIndex < 0)
            {
                errorProvider1.SetErrorWithCount(cmbAPIVersion, "Please select API version");
            }
            else
            {
                errorProvider1.SetErrorWithCount(cmbAPIVersion, string.Empty);
            }
            if (string.IsNullOrWhiteSpace(txtClientId.Text))
            {
                errorProvider1.SetErrorWithCount(txtClientId, "Client Id can not be empty");
            }
            else
            {
                errorProvider1.SetErrorWithCount(txtClientId, string.Empty);
            }
            if (string.IsNullOrWhiteSpace(txtPassKey.Text))
            {
                errorProvider1.SetErrorWithCount(txtPassKey, "Pass Key can not be empty");
            }
            else
            {
                errorProvider1.SetErrorWithCount(txtPassKey, string.Empty);
            }
            if (cmbPeriode.SelectedIndex < 0)
            {
                errorProvider1.SetErrorWithCount(cmbPeriode, "Please select periode");
            }
            else
            {
                errorProvider1.SetErrorWithCount(cmbPeriode, string.Empty);
            }
            if (string.IsNullOrWhiteSpace(txtMachineConfig.Text))
            {
                errorProvider1.SetErrorWithCount(txtMachineConfig, "Machine Config can not be empty");
            }
            else
            {
                errorProvider1.SetErrorWithCount(txtMachineConfig, string.Empty);
            }
            if (string.IsNullOrWhiteSpace(txtNIPConfig.Text))
            {
                errorProvider1.SetErrorWithCount(txtNIPConfig, "NIP Config can not be empty");
            }
            else
            {
                errorProvider1.SetErrorWithCount(txtNIPConfig, string.Empty);
            }
        }

        private void ClearInput()
        {
            errorProvider1.ReleaseAllError();
            lblTaskId.ResetText();
            txtTaskName.Enabled = false;
            //foreach (Control c in errorProvider1.ContainerControl.Controls)
            foreach (Control c in pnlAddTask.Controls)
            {
                if (c.GetType() == typeof(TextBox))
                {
                    TextBox txt = (TextBox)c;
                    txt.ResetText();
                }
                if (c.GetType() == typeof(ComboBox))
                {
                    ComboBox cmb = (ComboBox)c;
                    if (cmb.Items.Count > 0)
                        cmb.SelectedIndex = 0;
                }
                if (c.GetType() == typeof(NumericUpDown))
                {
                    NumericUpDown num = (NumericUpDown)c;
                    num.Value = 0;
                }
            }
            foreach (Control c in tabSource.Controls)
            {
                if (c.GetType() == typeof(TextBox))
                {
                    TextBox txt = (TextBox)c;
                    txt.ResetText();
                }
                if (c.GetType() == typeof(ComboBox))
                {
                    ComboBox cmb = (ComboBox)c;
                    if (cmb.Items.Count > 0)
                        cmb.SelectedIndex = 0;
                }
                if (c.GetType() == typeof(NumericUpDown))
                {
                    NumericUpDown num = (NumericUpDown)c;
                    num.Value = 0;
                }
            }
            foreach (Control c in tabDestination.Controls)
            {
                if (c.GetType() == typeof(TextBox))
                {
                    TextBox txt = (TextBox)c;
                    txt.ResetText();
                }
                if (c.GetType() == typeof(ComboBox))
                {
                    ComboBox cmb = (ComboBox)c;
                    if (cmb.Items.Count > 0)
                        cmb.SelectedIndex = 0;
                }
                if (c.GetType() == typeof(NumericUpDown))
                {
                    NumericUpDown num = (NumericUpDown)c;
                    num.Value = 0;
                }
            }
            foreach (Control c in tabSchedule.Controls)
            {
                if (c.GetType() == typeof(TextBox))
                {
                    TextBox txt = (TextBox)c;
                    txt.ResetText();
                }
                if (c.GetType() == typeof(ComboBox))
                {
                    ComboBox cmb = (ComboBox)c;
                    if (cmb.Items.Count > 0)
                        cmb.SelectedIndex = 0;
                }
                if (c.GetType() == typeof(NumericUpDown))
                {
                    NumericUpDown num = (NumericUpDown)c;
                    num.Value = 0;
                }
            }
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            FingerControlClass fcc = new FingerControlClass();
            NATUServices.NATUServicesClient _natuClient = new NATUServices.NATUServicesClient();
            try
            {
                string sxNewURI = "http://localhost:9940//NATUServices.svc";
                //string sxNewURI = "http://localhost/NATUServices.svc";
                _natuClient.Endpoint.Address = new EndpointAddress(new Uri(sxNewURI),
                    _natuClient.Endpoint.Address.Identity, _natuClient.Endpoint.Address.Headers);
                _natuClient.Open();
                ServicePoint servicePoint = ServicePointManager.FindServicePoint(new Uri(sxNewURI));
                if (servicePoint.Expect100Continue == true)
                { //should happen only once for each URL
                    servicePoint.Expect100Continue = false;
                }
                DataTable dt = new DataTable();
                dt.Columns.Add("NIP", typeof(string));
                dt.Columns.Add("LogDate", typeof(string));
                dt.Columns.Add("LogTime", typeof(string));
                dt.Columns.Add("LogType", typeof(string));
                dt.TableName = "Data";
                DataRow dr = dt.NewRow();
                dr["NIP"] = "12345556";
                dr["LogType"] = "1";
                dr["LogDate"] = DateTime.Now.ToString("yyyy-MM-dd");
                dr["LogTime"] = DateTime.Now.ToString("HH':'mm':'ss");
                dt.Rows.Add(dr);
                bool isSuccess = _natuClient.UploadData(txtClientId.Text, DateTime.Now, dt, 1, "123.1.1.1");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                //fcc.DisconnectDevice_Net(fd);
                _natuClient.Close();
                //_natuClient.Abort();
            }
            //FingerDevice fd = new FingerDevice();
            //fd.IPAddress = "172.31.32.212";
            //fd.Username = "1";
            //fd.Password = "123456";
            //FingerControlClass fcc = new FingerControlClass();
            //string sxErrorMessage = string.Empty;
            //string sxOutput = string.Empty;
            //UpdateTaskLastUpdatedDate(Convert.ToInt32(lblTaskId.Text));
            //try
            //{
            //    bool a = fd.Machine.SetCommPassword(123456);
            //    if (fcc.ConnectDevice_Net(fd, ref sxErrorMessage))
            //    {
            //        //bool a = fd.Machine.SetCommPassword(21); //set comm password for PC
            //        //fd.Machine.SetDeviceCommPwd(fd.Machine.MachineNumber, 22); //set comm password for device
            //        //not working
            //        //a = fd.Machine.SetCommPassword(77);
            //        int iValue = 0;
            //        fd.Machine.GetDeviceInfo(fd.Machine.MachineNumber, 2, ref iValue);//Device ID
            //        int iErrorCode = 0;
            //        fd.Machine.GetLastError(ref iErrorCode);
            //        if (fcc.TestAllFunction(fd, ref sxOutput, ref sxErrorMessage))
            //        {
            //            MessageBox.Show("OUTPUT: "+sxOutput);
            //        }
            //        else
            //        {
            //            MessageBox.Show("OUTPUT: "+sxOutput);
            //            throw new Exception(sxErrorMessage);
            //        }
            //    }
            //    else
            //    {
            //        throw new Exception(sxErrorMessage);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message);
            //}
            //finally
            //{
            //    fcc.DisconnectDevice_Net(fd);
            //}
        }

        private void UpdateTaskLastUpdatedDate(int iTaskId)
        {
            try
            {
                using (OleDbConnection con = new OleDbConnection(ConfigurationManager.ConnectionStrings["hcisConnectionString"].ConnectionString))
                {
                    con.Open();
                    string sxQuery = string.Empty;
                    OleDbCommand comm;
                    int iRowAffected = 0;
                    //sxQuery = @"
                    //     update hcis_NATU_task
                    //        set LastUploadDate = '" + DateTime.Now.ToString("yyyy-MM-dd") + @"'
                    //        where TaskId = " + iTaskId.ToString() + @";";
                    sxQuery = @"
	                        update hcis_NATU_task
                            set LastUploadDate = Date(), LastUploadTime = Time()
                            where TaskId = " + iTaskId.ToString() + @";";
                    comm = new OleDbCommand(sxQuery, con);
                    iRowAffected += comm.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    public static class ErrorProviderExtensions
    {
        private static int count;

        public static void SetErrorWithCount(this ErrorProvider ep, Control c, string message)
        {
            if (string.IsNullOrWhiteSpace(message))
            {
                if (!string.IsNullOrWhiteSpace(ep.GetError(c)))
                    count--;
            }
            else
            {
                count++;
                ep.SetError(c, message);
            }

        }

        public static bool HasErrors(this ErrorProvider ep)
        {
            return count != 0;
        }

        public static int GetErrorCount(this ErrorProvider ep)
        {
            return count;
        }

        public static void ReleaseAllError(this ErrorProvider ep)
        {
            ep.Clear();
        }
    }

    public class Absence
    {
        public Absence()
        { }
        public string NIP
        { get; set; }

        public int LogType
        { get; set; }
        public string LogDate
        { get; set; }
        public string LogTime
        { get; set; }

    }
}
