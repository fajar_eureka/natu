﻿namespace Netis.AttendanceTaskUploader.TaskSetting
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.pnlAddTask = new System.Windows.Forms.Panel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblTaskId = new System.Windows.Forms.Label();
            this.btnSaveTask = new System.Windows.Forms.Button();
            this.txtTaskName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabSource = new System.Windows.Forms.TabPage();
            this.chkDeleteAfterProcess = new System.Windows.Forms.CheckBox();
            this.label25 = new System.Windows.Forms.Label();
            this.radName = new System.Windows.Forms.RadioButton();
            this.radId = new System.Windows.Forms.RadioButton();
            this.label17 = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.txtIPAddress = new System.Windows.Forms.TextBox();
            this.cmbModel = new System.Windows.Forms.ComboBox();
            this.cmbManufacturer = new System.Windows.Forms.ComboBox();
            this.cmbType = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tabComparisonConfig = new System.Windows.Forms.TabPage();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.txtNIPConfig = new System.Windows.Forms.TextBox();
            this.txtMachineConfig = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.tabDestination = new System.Windows.Forms.TabPage();
            this.txtPassKey = new System.Windows.Forms.TextBox();
            this.txtClientId = new System.Windows.Forms.TextBox();
            this.cmbAPIVersion = new System.Windows.Forms.ComboBox();
            this.txtServerIPAddress = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tabSchedule = new System.Windows.Forms.TabPage();
            this.label16 = new System.Windows.Forms.Label();
            this.numMinutes = new System.Windows.Forms.NumericUpDown();
            this.numHours = new System.Windows.Forms.NumericUpDown();
            this.cmbPeriode = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.gvTaskList = new System.Windows.Forms.DataGridView();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.btnTest = new System.Windows.Forms.Button();
            this.pnlAddTask.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabSource.SuspendLayout();
            this.tabComparisonConfig.SuspendLayout();
            this.tabDestination.SuspendLayout();
            this.tabSchedule.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numMinutes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTaskList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(584, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Attendance Data Uploader";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Task List";
            // 
            // btnAdd
            // 
            this.btnAdd.AutoSize = true;
            this.btnAdd.Location = new System.Drawing.Point(88, 7);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 2;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // pnlAddTask
            // 
            this.pnlAddTask.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlAddTask.Controls.Add(this.btnCancel);
            this.pnlAddTask.Controls.Add(this.label2);
            this.pnlAddTask.Controls.Add(this.btnAdd);
            this.pnlAddTask.Controls.Add(this.lblTaskId);
            this.pnlAddTask.Controls.Add(this.btnSaveTask);
            this.pnlAddTask.Controls.Add(this.txtTaskName);
            this.pnlAddTask.Controls.Add(this.label3);
            this.pnlAddTask.Controls.Add(this.tabControl1);
            this.pnlAddTask.Location = new System.Drawing.Point(12, 44);
            this.pnlAddTask.Name = "pnlAddTask";
            this.pnlAddTask.Size = new System.Drawing.Size(560, 312);
            this.pnlAddTask.TabIndex = 3;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(465, 10);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Visible = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblTaskId
            // 
            this.lblTaskId.AutoSize = true;
            this.lblTaskId.Location = new System.Drawing.Point(4, 15);
            this.lblTaskId.Name = "lblTaskId";
            this.lblTaskId.Size = new System.Drawing.Size(0, 13);
            this.lblTaskId.TabIndex = 4;
            this.lblTaskId.Visible = true;
            // 
            // btnSaveTask
            // 
            this.btnSaveTask.Location = new System.Drawing.Point(384, 10);
            this.btnSaveTask.Name = "btnSaveTask";
            this.btnSaveTask.Size = new System.Drawing.Size(75, 23);
            this.btnSaveTask.TabIndex = 2;
            this.btnSaveTask.Text = "Save";
            this.btnSaveTask.UseVisualStyleBackColor = true;
            this.btnSaveTask.Visible = false;
            this.btnSaveTask.Click += new System.EventHandler(this.btnSaveTask_Click);
            // 
            // txtTaskName
            // 
            this.txtTaskName.Location = new System.Drawing.Point(173, 9);
            this.txtTaskName.MaxLength = 30;
            this.txtTaskName.Name = "txtTaskName";
            this.txtTaskName.Size = new System.Drawing.Size(189, 20);
            this.txtTaskName.TabIndex = 1;
            this.txtTaskName.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(99, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Task Name";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabSource);
            this.tabControl1.Controls.Add(this.tabComparisonConfig);
            this.tabControl1.Controls.Add(this.tabDestination);
            this.tabControl1.Controls.Add(this.tabSchedule);
            this.tabControl1.Location = new System.Drawing.Point(102, 46);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(357, 244);
            this.tabControl1.TabIndex = 3;
            // 
            // tabSource
            // 
            this.tabSource.Controls.Add(this.chkDeleteAfterProcess);
            this.tabSource.Controls.Add(this.label25);
            this.tabSource.Controls.Add(this.radName);
            this.tabSource.Controls.Add(this.radId);
            this.tabSource.Controls.Add(this.label17);
            this.tabSource.Controls.Add(this.txtPassword);
            this.tabSource.Controls.Add(this.txtUsername);
            this.tabSource.Controls.Add(this.txtIPAddress);
            this.tabSource.Controls.Add(this.cmbModel);
            this.tabSource.Controls.Add(this.cmbManufacturer);
            this.tabSource.Controls.Add(this.cmbType);
            this.tabSource.Controls.Add(this.label9);
            this.tabSource.Controls.Add(this.label8);
            this.tabSource.Controls.Add(this.label7);
            this.tabSource.Controls.Add(this.label6);
            this.tabSource.Controls.Add(this.label5);
            this.tabSource.Controls.Add(this.label4);
            this.tabSource.Location = new System.Drawing.Point(4, 22);
            this.tabSource.Name = "tabSource";
            this.tabSource.Padding = new System.Windows.Forms.Padding(3);
            this.tabSource.Size = new System.Drawing.Size(349, 218);
            this.tabSource.TabIndex = 0;
            this.tabSource.Text = "Source";
            this.tabSource.UseVisualStyleBackColor = true;
            // 
            // chkDeleteAfterProcess
            // 
            this.chkDeleteAfterProcess.AutoSize = true;
            this.chkDeleteAfterProcess.Location = new System.Drawing.Point(114, 195);
            this.chkDeleteAfterProcess.Name = "chkDeleteAfterProcess";
            this.chkDeleteAfterProcess.Size = new System.Drawing.Size(44, 17);
            this.chkDeleteAfterProcess.TabIndex = 16;
            this.chkDeleteAfterProcess.Text = "Yes";
            this.chkDeleteAfterProcess.UseVisualStyleBackColor = true;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(8, 196);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(104, 13);
            this.label25.TabIndex = 15;
            this.label25.Text = "Delete After Process";
            // 
            // radName
            // 
            this.radName.AutoSize = true;
            this.radName.Location = new System.Drawing.Point(168, 176);
            this.radName.Name = "radName";
            this.radName.Size = new System.Drawing.Size(53, 17);
            this.radName.TabIndex = 14;
            this.radName.TabStop = true;
            this.radName.Text = "Name";
            this.radName.UseVisualStyleBackColor = true;
            // 
            // radId
            // 
            this.radId.AutoSize = true;
            this.radId.Location = new System.Drawing.Point(114, 176);
            this.radId.Name = "radId";
            this.radId.Size = new System.Drawing.Size(36, 17);
            this.radId.TabIndex = 13;
            this.radId.TabStop = true;
            this.radId.Text = "ID";
            this.radId.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(8, 176);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(73, 13);
            this.label17.TabIndex = 12;
            this.label17.Text = "NIP in column";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(114, 149);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(198, 20);
            this.txtPassword.TabIndex = 11;
            this.txtPassword.UseSystemPasswordChar = true;
            // 
            // txtUsername
            // 
            this.txtUsername.Location = new System.Drawing.Point(114, 122);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(198, 20);
            this.txtUsername.TabIndex = 10;
            // 
            // txtIPAddress
            // 
            this.txtIPAddress.Location = new System.Drawing.Point(114, 95);
            this.txtIPAddress.Name = "txtIPAddress";
            this.txtIPAddress.Size = new System.Drawing.Size(198, 20);
            this.txtIPAddress.TabIndex = 9;
            // 
            // cmbModel
            // 
            this.cmbModel.FormattingEnabled = true;
            this.cmbModel.Location = new System.Drawing.Point(114, 68);
            this.cmbModel.Name = "cmbModel";
            this.cmbModel.Size = new System.Drawing.Size(198, 21);
            this.cmbModel.TabIndex = 8;
            // 
            // cmbManufacturer
            // 
            this.cmbManufacturer.FormattingEnabled = true;
            this.cmbManufacturer.Location = new System.Drawing.Point(114, 41);
            this.cmbManufacturer.Name = "cmbManufacturer";
            this.cmbManufacturer.Size = new System.Drawing.Size(198, 21);
            this.cmbManufacturer.TabIndex = 7;
            this.cmbManufacturer.SelectedIndexChanged += new System.EventHandler(this.cmbManufacturer_SelectedIndexChanged);
            // 
            // cmbType
            // 
            this.cmbType.FormattingEnabled = true;
            this.cmbType.Location = new System.Drawing.Point(114, 14);
            this.cmbType.Name = "cmbType";
            this.cmbType.Size = new System.Drawing.Size(198, 21);
            this.cmbType.TabIndex = 6;
            this.cmbType.SelectedIndexChanged += new System.EventHandler(this.cmbType_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 152);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 13);
            this.label9.TabIndex = 5;
            this.label9.Text = "Device Password";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 125);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Device Id";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 98);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "IP Address";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 71);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Model";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 44);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Manufacturer";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Type";
            // 
            // tabComparisonConfig
            // 
            this.tabComparisonConfig.Controls.Add(this.textBox1);
            this.tabComparisonConfig.Controls.Add(this.textBox2);
            this.tabComparisonConfig.Controls.Add(this.label23);
            this.tabComparisonConfig.Controls.Add(this.label24);
            this.tabComparisonConfig.Controls.Add(this.label22);
            this.tabComparisonConfig.Controls.Add(this.txtNIPConfig);
            this.tabComparisonConfig.Controls.Add(this.txtMachineConfig);
            this.tabComparisonConfig.Controls.Add(this.label21);
            this.tabComparisonConfig.Controls.Add(this.label20);
            this.tabComparisonConfig.Controls.Add(this.label19);
            this.tabComparisonConfig.Controls.Add(this.label18);
            this.tabComparisonConfig.Location = new System.Drawing.Point(4, 22);
            this.tabComparisonConfig.Name = "tabComparisonConfig";
            this.tabComparisonConfig.Padding = new System.Windows.Forms.Padding(3);
            this.tabComparisonConfig.Size = new System.Drawing.Size(349, 218);
            this.tabComparisonConfig.TabIndex = 3;
            this.tabComparisonConfig.Text = "Comparison Config";
            this.tabComparisonConfig.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(189, 102);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(125, 20);
            this.textBox1.TabIndex = 10;
            this.textBox1.Text = "OOOOOOXXXX";
            // 
            // textBox2
            // 
            this.textBox2.Enabled = false;
            this.textBox2.Location = new System.Drawing.Point(37, 102);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(125, 20);
            this.textBox2.TabIndex = 9;
            this.textBox2.Text = "XXXX";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(186, 82);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(94, 13);
            this.label23.TabIndex = 8;
            this.label23.Text = "NIP (2016041234)";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(34, 82);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(81, 13);
            this.label24.TabIndex = 7;
            this.label24.Text = "Machine (1234)";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(12, 62);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(47, 13);
            this.label22.TabIndex = 6;
            this.label22.Text = "Example";
            // 
            // txtNIPConfig
            // 
            this.txtNIPConfig.Location = new System.Drawing.Point(189, 168);
            this.txtNIPConfig.Name = "txtNIPConfig";
            this.txtNIPConfig.Size = new System.Drawing.Size(125, 20);
            this.txtNIPConfig.TabIndex = 5;
            // 
            // txtMachineConfig
            // 
            this.txtMachineConfig.Location = new System.Drawing.Point(37, 168);
            this.txtMachineConfig.Name = "txtMachineConfig";
            this.txtMachineConfig.Size = new System.Drawing.Size(125, 20);
            this.txtMachineConfig.TabIndex = 4;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(186, 142);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(25, 13);
            this.label21.TabIndex = 3;
            this.label21.Text = "NIP";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(34, 142);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(48, 13);
            this.label20.TabIndex = 2;
            this.label20.Text = "Machine";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(34, 37);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(153, 13);
            this.label19.TabIndex = 1;
            this.label19.Text = "Use \"X\" to represent used digit";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(34, 12);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(166, 13);
            this.label18.TabIndex = 0;
            this.label18.Text = "Use \"O\" to represent unused digit";
            // 
            // tabDestination
            // 
            this.tabDestination.Controls.Add(this.txtPassKey);
            this.tabDestination.Controls.Add(this.txtClientId);
            this.tabDestination.Controls.Add(this.cmbAPIVersion);
            this.tabDestination.Controls.Add(this.txtServerIPAddress);
            this.tabDestination.Controls.Add(this.label13);
            this.tabDestination.Controls.Add(this.label12);
            this.tabDestination.Controls.Add(this.label11);
            this.tabDestination.Controls.Add(this.label10);
            this.tabDestination.Location = new System.Drawing.Point(4, 22);
            this.tabDestination.Name = "tabDestination";
            this.tabDestination.Padding = new System.Windows.Forms.Padding(3);
            this.tabDestination.Size = new System.Drawing.Size(349, 218);
            this.tabDestination.TabIndex = 1;
            this.tabDestination.Text = "Destination";
            this.tabDestination.UseVisualStyleBackColor = true;
            // 
            // txtPassKey
            // 
            this.txtPassKey.Location = new System.Drawing.Point(137, 91);
            this.txtPassKey.Name = "txtPassKey";
            this.txtPassKey.Size = new System.Drawing.Size(177, 20);
            this.txtPassKey.TabIndex = 7;
            // 
            // txtClientId
            // 
            this.txtClientId.Location = new System.Drawing.Point(137, 67);
            this.txtClientId.Name = "txtClientId";
            this.txtClientId.Size = new System.Drawing.Size(177, 20);
            this.txtClientId.TabIndex = 6;
            // 
            // cmbAPIVersion
            // 
            this.cmbAPIVersion.FormattingEnabled = true;
            this.cmbAPIVersion.Location = new System.Drawing.Point(137, 43);
            this.cmbAPIVersion.Name = "cmbAPIVersion";
            this.cmbAPIVersion.Size = new System.Drawing.Size(177, 21);
            this.cmbAPIVersion.TabIndex = 5;
            // 
            // txtServerIPAddress
            // 
            this.txtServerIPAddress.Location = new System.Drawing.Point(137, 19);
            this.txtServerIPAddress.Name = "txtServerIPAddress";
            this.txtServerIPAddress.Size = new System.Drawing.Size(177, 20);
            this.txtServerIPAddress.TabIndex = 4;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(10, 94);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(51, 13);
            this.label13.TabIndex = 3;
            this.label13.Text = "Pass Key";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(10, 70);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(45, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "Client Id";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(10, 46);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(62, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "API Version";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(99, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Server\'s IP Address";
            // 
            // tabSchedule
            // 
            this.tabSchedule.Controls.Add(this.label16);
            this.tabSchedule.Controls.Add(this.numMinutes);
            this.tabSchedule.Controls.Add(this.numHours);
            this.tabSchedule.Controls.Add(this.cmbPeriode);
            this.tabSchedule.Controls.Add(this.label15);
            this.tabSchedule.Controls.Add(this.label14);
            this.tabSchedule.Location = new System.Drawing.Point(4, 22);
            this.tabSchedule.Name = "tabSchedule";
            this.tabSchedule.Padding = new System.Windows.Forms.Padding(3);
            this.tabSchedule.Size = new System.Drawing.Size(349, 218);
            this.tabSchedule.TabIndex = 2;
            this.tabSchedule.Text = "Schedule";
            this.tabSchedule.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(190, 58);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(10, 13);
            this.label16.TabIndex = 5;
            this.label16.Text = ":";
            // 
            // numMinutes
            // 
            this.numMinutes.Location = new System.Drawing.Point(213, 56);
            this.numMinutes.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.numMinutes.Name = "numMinutes";
            this.numMinutes.Size = new System.Drawing.Size(42, 20);
            this.numMinutes.TabIndex = 4;
            // 
            // numHours
            // 
            this.numHours.Location = new System.Drawing.Point(135, 56);
            this.numHours.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.numHours.Name = "numHours";
            this.numHours.Size = new System.Drawing.Size(42, 20);
            this.numHours.TabIndex = 3;
            // 
            // cmbPeriode
            // 
            this.cmbPeriode.FormattingEnabled = true;
            this.cmbPeriode.Location = new System.Drawing.Point(135, 23);
            this.cmbPeriode.Name = "cmbPeriode";
            this.cmbPeriode.Size = new System.Drawing.Size(121, 21);
            this.cmbPeriode.TabIndex = 2;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(14, 58);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(30, 13);
            this.label15.TabIndex = 1;
            this.label15.Text = "Time";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(14, 26);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(43, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Periode";
            // 
            // gvTaskList
            // 
            this.gvTaskList.AllowUserToAddRows = false;
            this.gvTaskList.AllowUserToResizeColumns = false;
            this.gvTaskList.AllowUserToResizeRows = false;
            this.gvTaskList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvTaskList.Location = new System.Drawing.Point(15, 81);
            this.gvTaskList.Name = "gvTaskList";
            this.gvTaskList.Size = new System.Drawing.Size(553, 254);
            this.gvTaskList.TabIndex = 4;
            this.gvTaskList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gvTaskList_CellClick);
            this.gvTaskList.Paint += new System.Windows.Forms.PaintEventHandler(this.gvTaskList_Paint);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // btnTest
            // 
            this.btnTest.Location = new System.Drawing.Point(15, 12);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(103, 23);
            this.btnTest.TabIndex = 5;
            this.btnTest.Text = "SAPU JAGAT";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Visible = false;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 361);
            this.Controls.Add(this.btnTest);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gvTaskList);
            this.Controls.Add(this.pnlAddTask);
            this.MaximumSize = new System.Drawing.Size(600, 400);
            this.MinimumSize = new System.Drawing.Size(600, 400);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Netis Attendance Task Uploader";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.pnlAddTask.ResumeLayout(false);
            this.pnlAddTask.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabSource.ResumeLayout(false);
            this.tabSource.PerformLayout();
            this.tabComparisonConfig.ResumeLayout(false);
            this.tabComparisonConfig.PerformLayout();
            this.tabDestination.ResumeLayout(false);
            this.tabDestination.PerformLayout();
            this.tabSchedule.ResumeLayout(false);
            this.tabSchedule.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numMinutes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTaskList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Panel pnlAddTask;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabSource;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.TextBox txtIPAddress;
        private System.Windows.Forms.ComboBox cmbModel;
        private System.Windows.Forms.ComboBox cmbManufacturer;
        private System.Windows.Forms.ComboBox cmbType;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabPage tabDestination;
        private System.Windows.Forms.TabPage tabSchedule;
        private System.Windows.Forms.Button btnSaveTask;
        private System.Windows.Forms.TextBox txtTaskName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPassKey;
        private System.Windows.Forms.TextBox txtClientId;
        private System.Windows.Forms.ComboBox cmbAPIVersion;
        private System.Windows.Forms.TextBox txtServerIPAddress;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown numMinutes;
        private System.Windows.Forms.NumericUpDown numHours;
        private System.Windows.Forms.ComboBox cmbPeriode;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.DataGridView gvTaskList;
        private System.Windows.Forms.Label lblTaskId;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.RadioButton radName;
        private System.Windows.Forms.RadioButton radId;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.TabPage tabComparisonConfig;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtNIPConfig;
        private System.Windows.Forms.TextBox txtMachineConfig;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.CheckBox chkDeleteAfterProcess;
        private System.Windows.Forms.Label label25;
    }
}

