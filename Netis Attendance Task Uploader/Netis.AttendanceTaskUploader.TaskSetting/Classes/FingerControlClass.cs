﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zkemkeeper;

namespace Netis.AttendanceTaskUploader.TaskSetting.Classes
{
    public class FingerControlClass
    {
        public bool ConnectDevice_Net(FingerDevice fd, ref string sxErrorMessage)
        {
            if (fd.isConnected)
            {
                fd.Machine.Disconnect();
            }

            fd.Machine.SetCommPassword(Convert.ToInt32(fd.Password));
            if (fd.Machine.Connect_Net(fd.IPAddress, fd.Port))
            {
                fd.isConnected = true;
                fd.Machine.RegEvent(fd.Machine.MachineNumber, 65535);
                return true;
            }
            else
            {
                int iErrorCode = 0;
                fd.Machine.GetLastError(ref iErrorCode);
                sxErrorMessage += "; failed to connect to device" + TranslateError(iErrorCode);
                return false;
            }
        }

        public void DisconnectDevice_Net(FingerDevice fd)
        {
            if(fd.isConnected)
            {
                fd.Machine.Disconnect();
                fd.isConnected = false;
            }
        }

        public DateTime GetDeviceTime(FingerDevice fd, ref string sxErrorMessage)
        {
            int iYear = 0;
            int iMonth = 0;
            int iDay = 0;
            int iHour = 0;
            int iMinute = 0;
            int iSecond = 0;
            int iErrorCode = -1;
            DateTime dtmReturn = DateTime.MinValue;
            fd.Machine.EnableDevice(fd.Machine.MachineNumber, false);
            bool a = fd.Machine.GetDeviceTime(fd.Machine.MachineNumber, ref iYear, ref iMonth, ref iDay, ref iHour, ref iMinute, ref iSecond);
            if (!a)
            {
                fd.Machine.GetLastError(ref iErrorCode);
                sxErrorMessage += "; failed to get device time" + TranslateError(iErrorCode);
            }
            else
            {
                dtmReturn = new DateTime(iYear, iMonth, iDay, iHour, iMinute, iSecond);
            }
            fd.Machine.EnableDevice(fd.Machine.MachineNumber, true);
            return dtmReturn;
        }

        public DataTable GetLogData(FingerDevice fd, int iNIPColumn, string sxMachineConfig, string sxNIPConfig, DataTable dtNIP, ref string sxErrorMessage)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("NIP", typeof(string));
            dt.Columns.Add("LogDate", typeof(string));
            dt.Columns.Add("LogTime", typeof(string));
            dt.Columns.Add("LogType", typeof(string));

            string EnrollNumber = null;
            string NIP = string.Empty;
            int dwVerifyMode = 0;
            int dwInOutMode = 0;
            string timeStr = null;
            int dwYear = 0;
            int dwMonth = 0;
            int dwDay = 0;
            int dwHour = 0;
            int dwMinute = 0;
            int dwSecond = 0;
            int dwWorkcode = 0;
            int dwReserved = 0;
            fd.Machine.EnableDevice(fd.Machine.MachineNumber, false);
            DataTable dtUserInfo = GetUserInfo(fd, ref sxErrorMessage);

            if (fd.Machine.ReadAllGLogData(fd.Machine.MachineNumber))
            {
                while (fd.Machine.SSR_GetGeneralLogData(fd.Machine.MachineNumber, out EnrollNumber, out dwVerifyMode, out dwInOutMode,
                    out dwYear, out dwMonth, out dwDay, out dwHour, out dwMinute, out dwSecond, ref dwWorkcode))
                {
                    if (iNIPColumn == 1)
                    {
                        NIP = EnrollNumber;
                    }
                    else
                    {
                        string sxSelect = "Id = '"+EnrollNumber+"'";
                        DataRow[] drResult = dtUserInfo.Select(sxSelect);
                        foreach (DataRow row in drResult)
                        {
                            if(row["Name"] != null || row["Name"] != DBNull.Value)
                            {
                                NIP = row["Name"].ToString();
                            }
                        }
                    }
                    string sxNIP = string.Empty;
                    sxNIP = NIP;// CompareNIP(sxMachineConfig, sxNIPConfig, NIP, dtNIP);
                    if(!string.IsNullOrWhiteSpace(sxNIP))
                    {
                        DataRow dr = dt.NewRow();
                        dr["NIP"] = sxNIP;
                        dr["LogType"] = dwInOutMode.ToString();
                        dr["LogDate"] = new DateTime(dwYear, dwMonth, dwDay, dwHour, dwMinute, dwSecond).ToString("yyyy-MM-dd");
                        dr["LogTime"] = new DateTime(dwYear, dwMonth, dwDay, dwHour, dwMinute, dwSecond).ToString("HH':'mm':'ss");
                        dt.Rows.Add(dr);
                    }
                    
                }
            }
            else
            {
                int iErrorCode = 0;
                fd.Machine.GetLastError(ref iErrorCode);
                sxErrorMessage += "; failed to get log data" + TranslateError(iErrorCode);
            }
            //fd.Machine.ClearGLog(fd.Machine.MachineNumber);
            fd.Machine.EnableDevice(fd.Machine.MachineNumber, true);
            return dt;
        }

        private string CompareNIP(string sxMachineConfig, string sxNIPConfig, string sxInputNIP, DataTable dtNIP)
        {
            string sxReturn = string.Empty;
            if(!string.IsNullOrWhiteSpace(sxInputNIP))
            {
                int iMachineLength = sxMachineConfig.Length >= sxInputNIP.Length ? sxMachineConfig.Length : sxInputNIP.Length;
                int iStartMachine = sxMachineConfig.Length >= sxInputNIP.Length ? sxMachineConfig.IndexOf("X") : 0;
                string sxMachineNIP = string.Empty;
                if(sxInputNIP.Length < iMachineLength)
                {
                    sxMachineNIP = sxInputNIP;
                }
                else
                {
                    sxMachineNIP = sxInputNIP.Substring(iStartMachine, iMachineLength - iStartMachine);
                }
                foreach (DataRow dr in dtNIP.Rows)
                {
                    string sxDataNIP = dr["NIP"].ToString();
                    int iNIPLength = sxNIPConfig.Length >= sxDataNIP.Length ? sxNIPConfig.Length : sxDataNIP.Length;
                    int iStartNIP = sxNIPConfig.Length >= sxDataNIP.Length ? sxNIPConfig.IndexOf("X") : 0;
                    //string sxNIP = sxDataNIP.Substring(iStartNIP, iNIPLength - iStartNIP);
                    string sxNIP = string.Empty;
                    if (sxDataNIP.Length < iNIPLength)
                    {
                        sxNIP = sxDataNIP;
                    }
                    else
                    {
                        sxNIP = sxDataNIP.Substring(iStartNIP, iNIPLength - iStartNIP);
                    }
                    if (sxMachineNIP.Equals(sxNIP))
                    {
                        sxReturn = sxDataNIP;
                        break;
                    }
                }
            }
            return sxReturn;
        }

        public bool MachineAuthentication(FingerDevice fd, ref string sxErrorMessage)
        {
            bool Enabled = false;
            DataTable dtUserInfo = GetUserInfo(fd, ref sxErrorMessage);
            if (dtUserInfo.Rows.Count > 0)
            {
                DataRow[] drResult = dtUserInfo.Select("Name = '" + fd.Username + "' and Password = '" + fd.Password + "'");
                if(drResult.Length > 0)
                {
                    foreach(DataRow dr in drResult)
                    {
                        Enabled = dr["Enabled"].ToString().Equals("1");
                    }
                    if(!Enabled)
                        sxErrorMessage += "; user and password is not authorized to access this machine";
                }
                else
                {
                    sxErrorMessage += "; user and password is not registered in this machine";
                }
            }
            else
            {
                sxErrorMessage += "; authentication failed";
            }
            return Enabled;
        }

        public DataTable GetUserInfo(FingerDevice fd, ref string sxErrorMessage)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Id", typeof(string));
            dt.Columns.Add("Name", typeof(string));
            dt.Columns.Add("Password", typeof(string));
            dt.Columns.Add("Privilege", typeof(string));
            dt.Columns.Add("Enabled", typeof(string));

            string EnrollNumber = null;
            string dwName = null;
            string dwPassword = null;
            int dwPrivilege = 0;
            bool dwEnabled = false;
            //fd.Machine.EnableDevice(fd.Machine.MachineNumber, false);

            if (fd.Machine.ReadAllGLogData(fd.Machine.MachineNumber))
            {
                while (fd.Machine.SSR_GetAllUserInfo(fd.Machine.MachineNumber, out EnrollNumber, out dwName, out dwPassword, out dwPrivilege, out dwEnabled))
                {
                    DataRow dr = dt.NewRow();
                    dr["Id"] = EnrollNumber;
                    dr["Name"] = dwName;
                    dr["Password"] = dwPassword.ToString();
                    dr["Privilege"] = dwPrivilege.ToString();
                    dr["Enabled"] = dwEnabled ? "1" : "0";
                    dt.Rows.Add(dr);
                }
            }
            else
            {
                int iErrorCode = 0;
                fd.Machine.GetLastError(ref iErrorCode);
                sxErrorMessage += "; failed to get user info " + TranslateError(iErrorCode);
            }
            //fd.Machine.ClearGLog(fd.Machine.MachineNumber);
            //fd.Machine.EnableDevice(fd.Machine.MachineNumber, true);
            return dt;
        }

        public bool ClearLogData(FingerDevice fd, ref string sxErrorMessage)
        {
            bool isSuccess = fd.Machine.ClearGLog(fd.Machine.MachineNumber);
            if (!isSuccess)
            {
                int iErrorCode = 0;
                fd.Machine.GetLastError(ref iErrorCode);
                sxErrorMessage += "; failed to clear log data" + TranslateError(iErrorCode);
            }
            return isSuccess;
        }

        public int GetDeviceId(FingerDevice fd, ref string sxErrorMessage)
        {
            int iValue = 0;
            if (!fd.Machine.GetDeviceInfo(fd.Machine.MachineNumber, 2, ref iValue))
            {
                int iErrorCode = 0;
                fd.Machine.GetLastError(ref iErrorCode);
                sxErrorMessage += " ; failed to get Device Id" + TranslateError(iErrorCode);
            }
            return iValue;
        }

        public bool TestAllFunction (FingerDevice fd, ref string sxOutput, ref string sxErrorMessage)
        {
            string sxOption = string.Empty;
            string sxValue = string.Empty;
            sxOutput = "COMMPORT: "+ fd.Machine.CommPort.ToString();

            sxOption = "~SSR";
            if (fd.Machine.GetSysOption(fd.Machine.MachineNumber, sxOption, out sxValue))
            {
                sxOutput += " ;SSR: " + sxValue;
            }
            else
            {
                int iErrorCode = 0;
                fd.Machine.GetLastError(ref iErrorCode);
                sxErrorMessage += " ; failed to get sys option ~SSR" + TranslateError(iErrorCode);
            }
            sxOption = "~ZKFPVersion";
            if (fd.Machine.GetSysOption(fd.Machine.MachineNumber, sxOption, out sxValue))
            {
                sxOutput += " ;ZKFPVersion: " + sxValue;
            }
            else
            {
                int iErrorCode = 0;
                fd.Machine.GetLastError(ref iErrorCode);
                sxErrorMessage += " ; failed to get sys option ~ZKFPVersion" + TranslateError(iErrorCode);
            }
            sxOption = "TCPPort";
            if (fd.Machine.GetSysOption(fd.Machine.MachineNumber, sxOption, out sxValue))
            {
                sxOutput += " ;TCPPort: " + sxValue;
            }
            else
            {
                int iErrorCode = 0;
                fd.Machine.GetLastError(ref iErrorCode);
                sxErrorMessage += " ; failed to get sys option TCPPort" + TranslateError(iErrorCode);
            }
            sxOption = "CommKey";
            if (fd.Machine.GetSysOption(fd.Machine.MachineNumber, sxOption, out sxValue))
            {
                sxOutput += " ;CommKey: " + sxValue;
            }
            else
            {
                int iErrorCode = 0;
                fd.Machine.GetLastError(ref iErrorCode);
                sxErrorMessage += " ; failed to get sys option CommKey" + TranslateError(iErrorCode);
            }
            sxOption = "CommPassword";
            if (fd.Machine.GetSysOption(fd.Machine.MachineNumber, sxOption, out sxValue))
            {
                sxOutput += " ;CommPassword: " + sxValue;
            }
            else
            {
                int iErrorCode = 0;
                fd.Machine.GetLastError(ref iErrorCode);
                sxErrorMessage += " ; failed to get sys option CommPassword" + TranslateError(iErrorCode);
            }
            if (fd.Machine.GetDeviceStrInfo(fd.Machine.MachineNumber, 1, out sxValue))
            {
                sxOutput += " ;DeviceStrInfo1: " + sxValue;
            }
            else
            {
                int iErrorCode = 0;
                fd.Machine.GetLastError(ref iErrorCode);
                sxErrorMessage += " ; failed to get device str info 1" + TranslateError(iErrorCode);
            }
            if (fd.Machine.GetSerialNumber(fd.Machine.MachineNumber, out sxValue))
            {
                sxOutput += " ;SerialNumber: " + sxValue;
            }
            else
            {
                int iErrorCode = 0;
                fd.Machine.GetLastError(ref iErrorCode);
                sxErrorMessage += " ; failed to get serial number" + TranslateError(iErrorCode);
            }
            if (fd.Machine.GetProductCode(fd.Machine.MachineNumber, out sxValue))
            {
                sxOutput += " ;product code: " + sxValue;
            }
            else
            {
                int iErrorCode = 0;
                fd.Machine.GetLastError(ref iErrorCode);
                sxErrorMessage += " ; failed to get product code" + TranslateError(iErrorCode);
            }

            return !string.IsNullOrEmpty(sxErrorMessage);
        }

        private string TranslateError(int iErrorCode)
        {
            switch (iErrorCode)
            {
                case 1:
                    return "SUCCEDED";
                case 4:
                    return "INVALID PARAMETER";
                case 0:
                    return "NO DATA FOUND";
                case 101:
                    return "BUFFER ALLOCATION ERROR";
                case -1:
                    return "DEVICE NOT INITIALIZED";
                case -2:
                    return "ERROR IO";
                case -3:
                    return "ERROR SIZE";
                case -4:
                    return "ERROR NOT ENOUGH SPACE";
                case -5:
                    return "ERROR DATA ALREADY EXISTS";
                case -7:
                    return "ERROR TIMEOUT";
                case -10:
                    return "ERROR INCORRECT DATA LENGTH";
                case -100:
                    return "ERROR OPERATION FAILED";
                default:
                    return "UNKNOWN ERROR";
            }
        }
    }

    public class FingerDevice
    {
        public CZKEM Machine { get; set; }
        public bool isConnected { get; set; }
        public int TypeId { get; set; }
        public string Type { get; set; }
        public int ManufacturerId { get; set; }
        public string Manufacturer { get; set; }
        public int ModelId { get; set; }
        public string Model { get; set; }
        public string IPAddress { get; set; }
        public int Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public FingerDevice()
        {
            Machine = new CZKEM();
            Port = 4370;
            isConnected = false;
        }
    }
}
