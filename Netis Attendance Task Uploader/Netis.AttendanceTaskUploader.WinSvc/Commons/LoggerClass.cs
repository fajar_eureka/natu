﻿using System;
using System.IO;

namespace Netis.AttendanceTaskUploader.WinSvc.Commons
{
    public static class LoggerClass
    {
        public static void WriteErrorLog(string sxMessage, string sxTimeLog)
        {
            string sxDirectoryPath = AppDomain.CurrentDomain.BaseDirectory + "NATULogFiles";
            try
            {
                if (!Directory.Exists(sxDirectoryPath))
                {
                    DirectoryInfo di = Directory.CreateDirectory(sxDirectoryPath);
                }

                string sxLogFilePath = sxDirectoryPath + "\\LogFiles(" + DateTime.Now.ToString("yyyy-MM-dd") + ").txt";
                if (!File.Exists(sxLogFilePath))
                {
                    FileStream fs = File.Create(sxLogFilePath);
                }
                FileInfo fi = new FileInfo(sxLogFilePath);
                using (StreamWriter stream = fi.AppendText())
                {
                    stream.WriteLine(sxTimeLog + " = " + sxMessage);
                }
            }
            catch { }
        }
    }
}
