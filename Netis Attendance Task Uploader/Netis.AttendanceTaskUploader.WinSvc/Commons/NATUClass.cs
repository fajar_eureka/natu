﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Netis.AttendanceTaskUploader.WinSvc.Commons
{
    public static class NATUClass
    {
        public static List<Tasks> GetTaskList()
        {
            List<Tasks> lsTasks = new List<Tasks>();
            using (OleDbConnection con = new OleDbConnection(ConfigurationManager.ConnectionStrings["hcisConnectionString"].ConnectionString))
            {
                con.Open();
                string sxQuery = @"
                    select 
	                    [TaskId],
	                    [TaskName],
	                    [LastUploadDate],
                        [SourceTypeId],
	                    [SourceType],
	                    [ManufacturerId],
	                    [Manufacturer],
	                    [ModelId],
	                    [Model],
	                    [SourceIPAddress],
	                    [SourceUsername],
	                    [SourcePassword],
                        [NIPColumn],
                        [MachineConfig],
                        [NIPConfig],
                        [DeleteAfterProcess],
	                    [DestinationIPAddress],
	                    [DestinationAPIVersion],
	                    [ClientId] as [DestinationClientId],
	                    [PassKey] as [DestinationPassKey],
	                    [PeriodeId],
	                    [Periode],
	                    [Hours],
	                    [Minutes]
                    from 
	                    vwTasks
                    where DeletedDate is null and OnOff = true;";
                OleDbDataAdapter da = new OleDbDataAdapter(sxQuery, con);
                DataTable dt = new DataTable();
                da.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Tasks objTask = new Tasks();
                        objTask.TaskId = Convert.ToInt32(dr["TaskId"]);
                        objTask.TaskName = dr["TaskName"].ToString();
                        objTask.LastUploadDate = dr["LastUploadDate"] == null || dr["LastUploadDate"] == DBNull.Value ?
                            DateTime.MinValue : Convert.ToDateTime(dr["LastUploadDate"]);
                        objTask.objFingerDevice = new FingerDevice();
                        objTask.objFingerDevice.TypeId = Convert.ToInt32(dr["SourceTypeId"]);
                        objTask.objFingerDevice.Type = dr["SourceType"].ToString();
                        objTask.objFingerDevice.ManufacturerId = Convert.ToInt32(dr["ManufacturerId"]);
                        objTask.objFingerDevice.Manufacturer = dr["Manufacturer"].ToString();
                        objTask.objFingerDevice.ModelId = Convert.ToInt32(dr["ModelId"]);
                        objTask.objFingerDevice.Model = dr["Model"].ToString();
                        objTask.objFingerDevice.IPAddress = dr["SourceIPAddress"].ToString();
                        objTask.objFingerDevice.Username = dr["SourceUsername"].ToString();
                        objTask.objFingerDevice.Password = dr["SourcePassword"].ToString();
                        objTask.DestinationIPAddress = dr["DestinationIPAddress"].ToString();
                        objTask.DestinationAPIVersion = Convert.ToInt32(dr["DestinationAPIVersion"]);
                        objTask.DestinationClientId = dr["DestinationClientId"].ToString();
                        objTask.DestinationPassKey = dr["DestinationPassKey"].ToString();
                        objTask.PeriodeId = Convert.ToInt32(dr["PeriodeId"]);
                        objTask.Periode = dr["Periode"].ToString();
                        objTask.UploadTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, Convert.ToInt16(dr["Hours"]), Convert.ToInt16(dr["Minutes"]), 0);
                        objTask.NIPColumn = Convert.ToInt32(dr["NIPColumn"]);
                        objTask.MachineConfig = dr["MachineConfig"].ToString();
                        objTask.NIPConfig = dr["NIPConfig"].ToString();
                        objTask.DeleteAfterProcess = Convert.ToBoolean(dr["DeleteAfterProcess"]);
                        lsTasks.Add(objTask);
                    }
                }

                foreach (Tasks _task in lsTasks)
                {
                    DateTime dtmLastUploadDate = new DateTime(_task.LastUploadDate.Year, _task.LastUploadDate.Month, _task.LastUploadDate.Day,
                        _task.LastUploadDate.Hour, _task.LastUploadDate.Minute, 0);

                    if (dtmLastUploadDate != DateTime.MinValue)
                    {
                        TimeSpan ts = _task.UploadTime.Subtract(dtmLastUploadDate);
                        switch (_task.Periode.ToLower())
                        {
                            case "daily":
                                if (!(ts.TotalHours == 24))
                                    lsTasks.Remove(_task);
                                break;
                            case "weekly":
                                if (!(ts.TotalDays == 7))
                                    lsTasks.Remove(_task);
                                break;
                            case "monthly":
                                if (_task.UploadTime.Month - dtmLastUploadDate.Month != 1)
                                    lsTasks.Remove(_task);
                                break;
                        }
                    }
                }
                return lsTasks;
            }
        }

        public static void UploadTask(Tasks obj)
        {
            FingerControlClass fcc = new FingerControlClass();
                NATUServices.NATUServicesClient _natuClient = new NATUServices.NATUServicesClient();
            try
            {
                string sxNewURI = "http://" + obj.DestinationIPAddress + "/NATUServices.svc";
                _natuClient.Endpoint.Address = new EndpointAddress(new Uri(sxNewURI),
                    _natuClient.Endpoint.Address.Identity, _natuClient.Endpoint.Address.Headers);
                _natuClient.Open();
                ServicePoint servicePoint = ServicePointManager.FindServicePoint(new Uri(sxNewURI));
                if (servicePoint.Expect100Continue == true)
                { //should happen only once for each URL
                    servicePoint.Expect100Continue = false;
                }
                bool isValid = _natuClient.ClientValidation(obj.DestinationClientId, obj.DestinationPassKey);
                int iNIPColumn = obj.NIPColumn;
                if (isValid)
                {
                    bool isSuccess = false;
                    bool isSuccess2 = false;
                    bool isSuccessNIP = false;
                    string sxErrorMessage = string.Empty;
                    DataTable dtNIP = new DataTable();//_natuClient.GetPersonsNIP();
                    if (fcc.ConnectDevice_Net(obj.objFingerDevice, ref sxErrorMessage))
                    {
                        //if (fcc.MachineAuthentication(obj.objFingerDevice, ref sxErrorMessage))
                        //{
                        int iDeviceId = fcc.GetDeviceId(obj.objFingerDevice, ref sxErrorMessage);
                        isSuccessNIP = _natuClient.UpdateNIPConfig(obj.DestinationClientId, obj.NIPConfig, obj.MachineConfig);
                        DataTable dt = fcc.GetLogData(obj.objFingerDevice, obj.NIPColumn, obj.MachineConfig, obj.NIPConfig, dtNIP, ref sxErrorMessage);
                        dt.TableName = "Data";
                        //isSuccess = _natuClient.UploadData(obj.DestinationClientId, DateTime.Now, dt, iDeviceId, obj.objFingerDevice.IPAddress);
                        //isSuccess = _natuClient.UploadData(txtClientId.Text, DateTime.Now, dt, iDeviceId, fd.IPAddress);
                        string sxMessage = _natuClient.UploadDataWithErrorMessage(obj.DestinationClientId, DateTime.Now, dt, iDeviceId, obj.objFingerDevice.IPAddress);
                        isSuccess = sxMessage.Contains("SUCCESS");
                        if (obj.DeleteAfterProcess)
                        {
                            isSuccess2 = fcc.ClearLogData(obj.objFingerDevice, ref sxErrorMessage);
                        }
                        else
                        {
                            isSuccess2 = true;
                        }
                        if (isSuccess && isSuccess2 && isSuccessNIP)
                        {
                            Commons.LoggerClass.WriteErrorLog("Log Data has been uploaded successfully", DateTime.Now.ToString());
                            UpdateTaskLastUpdatedDate(obj);
                            fcc.DisconnectDevice_Net(obj.objFingerDevice);
                        }
                        else
                        {
                            if (!isSuccess)
                            {
                                //sxErrorMessage += "error in web service(data count: "+dt.Rows.Count.ToString()+"); ";
                                sxErrorMessage += "message from web service: " + sxMessage.Split(new string[] { "]=[" }, StringSplitOptions.None)[1];
                            }
                            if (!isSuccess2)
                            {
                                sxErrorMessage += "error in clear log";
                            }
                            if (!isSuccessNIP)
                            {
                                sxErrorMessage += "error in UpdateNIPConfig";
                            }
                            throw new Exception("Error when uploading data : " + sxErrorMessage);
                        }
                        //}
                        //else
                        //{
                        //    throw new Exception("this username is not authorize to access this machine : " + sxErrorMessage);
                        //}
                    }
                    else
                    {
                        throw new Exception(sxErrorMessage);
                    }
                }
                else
                {
                    throw new Exception("Finger Device is not valid or is not active anymore");
                }

            }
            catch (Exception ex)
            {
                fcc.DisconnectDevice_Net(obj.objFingerDevice);
                _natuClient.Abort();
                throw ex;
            }
        }

        private static void UpdateTaskLastUpdatedDate(Tasks obj)
        {
            try
            {
                using (OleDbConnection con = new OleDbConnection(ConfigurationManager.ConnectionStrings["hcisConnectionString"].ConnectionString))
                {
                    con.Open();
                    string sxQuery = string.Empty;
                    OleDbCommand comm;
                    int iRowAffected = 0;
                    //sxQuery = @"
                    //     update hcis_NATU_task
                    //        set LastUploadDate = '" + DateTime.Now.ToString("yyyy-MM-dd") + @"'
                    //        where TaskId = " + obj.TaskId.ToString() + @";";
                    sxQuery = @"
	                        update hcis_NATU_task
                            set LastUploadDate = Date(), LastUploadTime = Time()
                            where TaskId = " + obj.TaskId.ToString() + @";";
                    comm = new OleDbCommand(sxQuery, con);
                    iRowAffected += comm.ExecuteNonQuery();

                    if (iRowAffected > 0)
                    {
                        Commons.LoggerClass.WriteErrorLog("Success to update LastUploadDate of : " + obj.TaskName, DateTime.Now.ToString());
                    }
                    else
                    {
                        Commons.LoggerClass.WriteErrorLog("Failed to update LastUploadDate of : " + obj.TaskName, DateTime.Now.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    public class Tasks
    {
        public int TaskId { get; set; }
        public string TaskName { get; set; }
        public DateTime LastUploadDate { get; set; }
        public FingerDevice objFingerDevice { get; set; }
        public string DestinationIPAddress { get; set; }
        public int DestinationAPIVersion { get; set; }
        public string DestinationClientId { get; set; }
        public string DestinationPassKey { get; set; }
        public int PeriodeId { get; set; }
        public string Periode { get; set; }
        public DateTime UploadTime { get; set; }
        public int NIPColumn { get; set; }
        public string MachineConfig { get; set; }
        public string NIPConfig { get; set; }
        public bool DeleteAfterProcess { get; set; }
    }

}
