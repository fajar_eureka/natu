﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Netis.AttendanceTaskUploader.WinSvc.Commons;

namespace Netis.AttendanceTaskUploader.WinSvc
{
    public partial class NATUWinService : ServiceBase
    {
        private Timer timer1 = new Timer();
        public NATUWinService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            Process();
        }
        public void Process()
        {
            timer1.Elapsed += new System.Timers.ElapsedEventHandler(timer1_Tick);
            timer1.Interval = 180000;
            timer1.Enabled = true;
            Commons.LoggerClass.WriteErrorLog("NATU Windows Service has been started", DateTime.Now.ToString());
            timer1.Start();
        }
        private void timer1_Tick(object sender, ElapsedEventArgs e)
        {
            Commons.LoggerClass.WriteErrorLog("========== Start Ticking ============", DateTime.Now.ToString());
            //Write the job here
            //Find the schedule first
            try
            {
                List<Tasks> lsTask = Commons.NATUClass.GetTaskList();
                Commons.LoggerClass.WriteErrorLog("Get Task List Count: "+lsTask.Count.ToString(), DateTime.Now.ToString());
                //List All Machine tobe Uploaded
                foreach (Tasks objTask in lsTask)
                {
                    if (objTask.UploadTime == DateTime.Now)
                    {
                        //Do the upload (same as "NOW" in desktop version)
                        Commons.NATUClass.UploadTask(objTask);
                    }
                }
            }
            catch(Exception ex)
            {
                Commons.LoggerClass.WriteErrorLog("Error : " + ex.Message, DateTime.Now.ToString());
            }
            Commons.LoggerClass.WriteErrorLog("=========== Stop Ticking ===========", DateTime.Now.ToString());
        }

        protected override void OnStop()
        {
            timer1.Enabled = false;
            Commons.LoggerClass.WriteErrorLog("NATU Windows Service has been stopped", DateTime.Now.ToString());
        }
    }
}
